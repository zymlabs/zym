<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\FrameworkBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\UnitOfWork;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Acl\Domain\AclCollectionCache;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Exception\NotAllAclsFoundException;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Zym\Bundle\FrameworkBundle\Repository\AbstractEntityRepository;
use Zym\Bundle\FrameworkBundle\Repository\PageableRepositoryInterface;

/**
 * Abstract Entity Manager
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
abstract class AbstractEntityManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var AbstractEntityRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var MutableAclProviderInterface
     */
    protected $aclProvider;

    /**
     * @var AclCollectionCache
     */
    protected $aclCollectionCache;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $class,
        PaginatorInterface $paginator,
        AclProviderInterface $aclProvider,
        TokenStorageInterface $tokenStorage = null,
        AclCollectionCache $aclCollectionCache = null
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository($class);;

        $metadata = $entityManager->getClassMetadata($class);
        $this->class = $metadata->getName();

        if ($this->repository instanceof PageableRepositoryInterface) {
            $this->repository->setPaginator($paginator);
        }

        $this->aclProvider = $aclProvider;

        if ($tokenStorage) {
            $this->tokenStorage = $tokenStorage;
        }

        if ($aclCollectionCache) {
            $this->aclCollectionCache = $aclCollectionCache;
        }
    }

    /**
     * @param array $criteria
     * @return object
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return MutableAclProviderInterface
     */
    public function getAclProvider()
    {
        return $this->aclProvider;
    }

    /**
     * @return AclCollectionCache
     */
    public function getAclCollectionCache()
    {
        return $this->aclCollectionCache;
    }

    /**
     * @return TokenStorageInterface
     */
    public function getSecurityTokenStorage()
    {
        return $this->tokenStorage;
    }

    /**
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function createEntity($entity)
    {
        $this->entityManager->beginTransaction();

        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            $oid = ObjectIdentity::fromDomainObject($entity);

            try {
                $acl = $this->aclProvider->createAcl($oid);
                $this->aclProvider->updateAcl($acl);
            } catch (AclAlreadyExistsException $e) {
                // No need to do anything
            }

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function deleteEntity($entity)
    {
        $this->entityManager->beginTransaction();

        try {
            $oid = ObjectIdentity::fromDomainObject($entity);
            $this->aclProvider->deleteAcl($oid);

            $this->entityManager->remove($entity);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * @param object $entity
     * @param bool $andFlush
     * @return object|null
     */
    protected function saveEntity($entity, bool $andFlush = true)
    {
        if ($this->entityManager->getUnitOfWork()->getEntityState($entity) === UnitOfWork::STATE_NEW) {
            return $this->createEntity($entity);
        } else {
            $this->entityManager->persist($entity);
            if ($andFlush) {
                $this->entityManager->flush();
            }
        }

        return $entity;
    }

    /**
     * @param Collection $entities
     */
    protected function loadAcls($entities)
    {
        $aclCollectionCache = $this->getAclCollectionCache();

        try {
            if ($aclCollectionCache) {
                $sortedEntities = [];
                foreach ($entities as $entity) {
                    $sortedEntities[get_class($entity)][] = $entity;
                }
                foreach ($sortedEntities as $entitiesGroup) {
                    if ($token = $this->tokenStorage->getToken()) {
                        $aclCollectionCache->cache($entitiesGroup, [$token]);
                    } else {
                        $aclCollectionCache->cache($entitiesGroup);
                    }
                }
            }
        } catch (NotAllAclsFoundException $e) {
            // At least we tried...
        } catch (AclNotFoundException $e) {
            // At least we tried...
        }
    }
}
