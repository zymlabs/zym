<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Zym\Bundle\ResqueBundle\Resque;

class PruneDeadWorkersCommand extends Command
{
    protected static $defaultName = 'zym:resque:prune-dead-workers';

    /**
     * @var Resque
     */
    private $resque;

    public function __construct(Resque $resque)
    {
        $this->resque = $resque;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setDescription('Prune dead workers');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Resque');

        $io->note('Pruning dead workers...');

        $this->resque->pruneDeadWorkers();

        $io->success('Done!');
    }
}
