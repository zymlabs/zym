<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RouterBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\RouterBundle\Entity\Route as RouteEntity;
use Zym\Bundle\RouterBundle\Form\DeleteType;
use Zym\Bundle\RouterBundle\Form\RouteType;
use Zym\Bundle\RouterBundle\Manager\RouteManager;

/**
 * Routes Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class RoutesController extends Controller
{
    /**
     * @var RouteManager
     */
    private $routeManager;

    public function __construct(RouteManager $routeManager)
    {
        $this->routeManager = $routeManager;
    }

    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_router_routes",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $routes = $this->routeManager->findRoutes($filterBy, $page, $limit, $orderBy);

        return [
            'routes' => $routes,
        ];
    }

    /**
     * @Route("/add", name="zym_router_routes_add")
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('CREATE', new ObjectIdentity('class', RouteEntity::class))) {
            throw new AccessDeniedException();
        }

        $route = new RouteEntity(null, null);
        $form = $this->createForm(RouteType::class, $route);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->routeManager->createRoute($route);

                return $this->redirectToRoute('zym_router_routes');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Edit a route
     *
     * @Route("/{name}/edit", name="zym_router_routes_edit")
     * @ParamConverter("route", class="ZymRouterBundle:Route")
     * @Template()
     *
     * @SecureParam(name="route", permissions="EDIT")
     *
     * @param Request $request
     * @param Route $route
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Route $route)
    {
        $origRoute = clone $route;
        $form = $this->createForm(RouteType::class, $route);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->routeManager->saveRoute($route);

                return $this->redirectToRoute('zym_router_routes');
            }
        }

        return [
            'route' => $origRoute,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a route
     *
     * @Route(
     *     "/{name}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     * @Route(
     *     "/{name}/delete.{_format}",
     *     name="zym_router_routes_delete",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="route", permissions="DELETE")
     *
     * @param Request $request
     * @param Route   $route
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request, Route $route)
    {
        $origRoute = clone $route;

        $form = $this->createForm(DeleteType::class, $route);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->routeManager->deleteRoute($route);

                return $this->redirectToRoute('zym_router_routes');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $this->routeManager->deleteRoute($route);

            return $this->redirectToRoute('zym_router_routes');
        }

        return [
            'route' => $origRoute,
            'form' => $form->createView(),
        ];
    }

    /**
     * Show a route
     *
     * @Route(
     *     "/{name}.{_format}",
     *     name     ="zym_router_routes_show",
     *     defaults = { "_format" = "html" }
     * )
     * @ParamConverter("route", class="ZymRouterBundle:Route")
     * @Template()
     *
     * @SecureParam(name="route", permissions="VIEW")
     *
     * @param Route $route
     * @return array
     */
    public function showAction(Route $route)
    {
        return [
            'route' => $route,
        ];
    }
}
