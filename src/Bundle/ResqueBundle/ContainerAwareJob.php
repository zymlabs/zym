<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ContainerAwareJob extends AbstractJob
{
    /**
     * @var KernelInterface
     */
    private $kernel = null;

    /**
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        if ($this->kernel === null) {
            $this->kernel = $this->createKernel();
            $this->kernel->boot();
        }

        return $this->kernel->getContainer();
    }

    /**
     * Set kernel options
     *
     * @param array $kernelOptions
     */
    public function setKernelOptions(array $kernelOptions)
    {
        $this->args = array_merge($this->args, $kernelOptions);
    }

    /**
     * Get kernel
     *
     * @return KernelInterface
     */
    public function getKernel()
    {
        return $this->kernel;
    }

    /**
     * Set kernel
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @return KernelInterface
     */
    protected function createKernel()
    {
        $finder = Finder::create()->files()->name('*Kernel.php')->depth(0)->in($this->args['kernel.root_dir']);

        $results = iterator_to_array($finder);
        $file    = current($results);
        $class = $file->getBasename('.php');

        require_once $file;

        if (!class_exists($class)) {
            $class = sprintf('App\\%s', $class);
        }

        return new $class(
            $this->args['kernel.environment'] ?? 'prod',
            filter_var($this->args['kernel.debug'] ?? false, FILTER_VALIDATE_BOOLEAN)
        );
    }

    public function tearDown()
    {
        if ($this->kernel) {
            $this->kernel->shutdown();
        }
    }
}
