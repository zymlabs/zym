<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Zym\Bundle\UserBundle\Model\TimeZoneInterface;

/**
 * Class TwigDateListener
 *
 * @package Zym\Bundle\UserBundle\EventListener
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class TwigDateListener
{
    /**
     * @var \Twig_Environment
     */
    protected $twigEnvironment;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    function __construct(\Twig_Environment $twigEnvironment, TokenStorageInterface $tokenStorage = null)
    {
        $this->twigEnvironment = $twigEnvironment;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($this->tokenStorage &&
            ($token = $this->tokenStorage->getToken()) instanceof TokenInterface &&
            ($user = $token->getUser()) instanceof TimeZoneInterface &&
            ($timeZone = $user->getTimezone()) &&
            ($coreExtension = $this->twigEnvironment->getExtension(\Twig_Extension_Core::class)) instanceof \Twig_Extension_Core) {
            $coreExtension->setTimezone($timeZone);
        }
    }
}
