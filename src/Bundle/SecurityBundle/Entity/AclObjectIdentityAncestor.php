<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acl Object Identity Ancestors
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 *
 * ORM\Entity()
 * @ORM\Table(
 *     name="acl_object_identity_ancestors",
 *     indexes={
 *         @ORM\Index(columns={"object_identity_id"}),
 *         @ORM\Index(columns={"ancestor_id"})
 *     }
 * )
 */
class AclObjectIdentityAncestor
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AclObjectIdentity")
     * @ORM\JoinColumn(name="object_identity_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $objectIdentity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AclObjectIdentity")
     * @ORM\JoinColumn(name="ancestor_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $ancestor;
}
