<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Form;

use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;

class AclSecurityIdentityEntityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['multiple']) {
            $builder->addEventListener(FormEvents::SUBMIT, function(Event $event) use ($options){
                $event->stopPropagation();
            }, 4);
        }

        $builder->resetViewTransformers();

        if ($options['multiple']) {
            $builder->addViewTransformer(new AclSecurityIdentityToArrayTransformer($options['choice_list']));
        } else {
            $builder->addViewTransformer(new AclSecurityIdentityToStringTransformer($options['choice_list']));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class'             => AclSecurityIdentity::class,
            'property'          => 'identifier',
            'multiple'          => true,
            'query_builder'     => function(ObjectRepository $er) {
                return $er->createQueryBuilder('r')
                          ->where('r.username = 0');
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return EntityType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
