<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Manager;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Repository\AclClassRepository;

/**
 * Acl Security Identity Manager
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class AclClassManager extends AbstractEntityManager
{
    /**
     * @var AclClassRepository
     */
    protected $repository;

    /**
     * @param AclClass $aclClass
     * @return AclClass
     */
    public function createAclClass(AclClass $aclClass)
    {
        return $this->createEntity($aclClass);
    }

    /**
     * @param AclClass $aclClass
     */
    public function deleteAclClass(AclClass $aclClass)
    {
        $this->deleteEntity($aclClass);
    }

    /**
     * @param AclClass $aclClass
     * @param bool $andFlush
     * @return AclClass
     */
    public function saveAclClass(AclClass $aclClass, bool $andFlush = true)
    {
        return $this->saveEntity($aclClass, $andFlush);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findAclClasses(array $criteria = [], int $page = 1, int $limit = 50, array $orderBy = [])
    {
        return $this->repository->findAclClasses($criteria, $page, $limit, $orderBy);
    }

    /**
     * @param array $criteria
     * @return AclClass|null
     */
    public function findAclClassBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }
}
