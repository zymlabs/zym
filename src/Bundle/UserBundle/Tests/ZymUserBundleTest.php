<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Tests;

use PHPUnit\Framework\TestCase;
use Zym\Bundle\UserBundle\ZymUserBundle;

/**
 * Zym User Bundle
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class ZymUserBundleTest extends TestCase
{
    /**
     *
     * @var ZymUserBundle
     */
    private $bundle;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->bundle = new ZymUserBundle();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        $this->bundle = null;
    }

    public function testGetParentReturnsFOS()
    {
        $this->assertEquals($this->bundle->getParent(), 'FOSUserBundle');
    }
}
