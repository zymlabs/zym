<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Zym\Bundle\UserBundle\Entity\User;

/**
 * Class UserFixtures
 *
 * @package Zym\Bundle\UserBundle\DataFixtures\ORM
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class UserFixtures extends Fixture
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user->setUsername('root');
        $user->setPlainPassword('admin');
        $user->setEmail('root@localhost');
        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->addRole('ROLE_USER');
        $user->addRole('ROLE_ADMIN');
        $user->addRole('ROLE_SUPER_ADMIN');
        $user->addRole('ROLE_ALLOWED_TO_SWITCH');

        $this->userManager->updateUser($user);
    }
}
