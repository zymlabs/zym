<?php

if (!isset($logger) || !is_object($logger)) {
    $logger = new Resque_Log(isset($logLevel) ? $logLevel : false);
}

// If set, re-attach failed jobs based on retry_strategy
Resque_Event::listen('onFailure', function(Exception $exception, Resque_Job $job) use ($logger) {
    $args = $job->getArguments();

    if (empty($args['zym_resque.retry_strategy'])) {
        return;
    }

    if (!isset($args['zym_resque.retry_attempt'])) {
        $args['zym_resque.retry_attempt'] = 0;
    }

    $backoff = $args['zym_resque.retry_strategy'];
    if (!isset($backoff[$args['zym_resque.retry_attempt']])) {
        return;
    }

    $delay = $backoff[$args['zym_resque.retry_attempt']];
    $args['zym_resque.retry_attempt']++;

    if ($delay == 0) {
        Resque::enqueue($job->queue, $job->payload['class'], $args);
        $logger->log(Psr\Log\LogLevel::ERROR, 'Job failed. Auto re-queued, attempt number: {attempt}', array(
            'attempt' => $args['zym_resque.retry_attempt'] - 1)
        );
    } else {
        $at = time() + $delay;
        ResqueScheduler::enqueueAt($at, $job->queue, $job->payload['class'], $args);

        $logger->log(Psr\Log\LogLevel::ERROR, 'Job failed. Auto re-queued. Scheduled for: {timestamp}, attempt number: {attempt}', array(
            'timestamp' => date('Y-m-d H:i:s', $at),
            'attempt'   => $args['zym_resque.retry_attempt'] - 1,
        ));
    }
});
