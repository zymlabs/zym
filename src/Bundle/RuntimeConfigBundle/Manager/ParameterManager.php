<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RuntimeConfigBundle\Manager;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\RuntimeConfigBundle\Entity\Parameter;
use Zym\Bundle\RuntimeConfigBundle\Repository\ParameterRepository;

/**
 * Parameter Manager
 *
 * @package Zym\Bundle\UserBundle
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ParameterManager extends AbstractEntityManager
{
    /**
     * @var ParameterRepository
     */
    protected $repository;

    /**
     * @param Parameter $parameter
     * @return Parameter
     */
    public function createParameter(Parameter $parameter)
    {
        parent::createEntity($parameter);

        return $parameter;
    }

    /**
     * @param Parameter $parameter
     * @param bool $andFlush
     */
    public function saveParameter(Parameter $parameter, bool $andFlush = true)
    {
        parent::saveEntity($parameter, $andFlush);
    }

    /**
     * @param Parameter $parameter
     */
    public function deleteParameter(Parameter $parameter)
    {
        parent::deleteEntity($parameter);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findParameters(array $criteria = [], int $page = 1, int $limit = 50, array $orderBy = [])
    {
        return $this->repository->findParameters($criteria, $page, $limit, $orderBy);
    }

    /**
     * @param array $criteria
     * @return Parameter|null
     */
    public function findParameterBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $name
     * @return Parameter|null
     */
    public function findParameter(string $name)
    {
        return $this->repository->findOneBy(['name' => $name]);
    }
}
