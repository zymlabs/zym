<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Zym\Bundle\MediaBundle\Model\AbstractMedia;
use Zym\Bundle\MediaBundle\Model\MediaInterface;

/**
 * @ORM\Entity(repositoryClass="MediaRepository")
 * @ORM\Table(name="media")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="object_type", type="string")
 * @ORM\HasLifecycleCallbacks()
 */
class Media extends AbstractMedia implements MediaInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=1024, nullable=true)
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_name", type="string")
     */
    protected $providerName;

    /**
     * @var int
     *
     * @ORM\Column(name="provider_status", type="integer")
     */
    protected $providerStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_reference", type="string")
     */
    protected $providerReference;

    /**
     * @var string
     */
    protected $previousProviderReference;

    /**
     * @var array
     *
     * @ORM\Column(name="provider_metadata", type="json", nullable=true)
     */
    protected $providerMetadata = [];

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $width;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $height;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", nullable=true)
     */
    protected $length;

    /**
     * @var string
     *
     * @ORM\Column(name="content_type", type="string", nullable=true, length=64)
     */
    protected $contentType;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $size;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $copyright;

    /**
     * @var string
     *
     * @ORM\Column(name="author_name", type="string", nullable=true)
     */
    protected $authorName;

    /**
     * @var mixed
     */
    protected $binaryContent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=16)
     */
    protected $context;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", name="cdn_flushable", nullable=true)
     */
    protected $cdnFlushable = false;

    /**
     * @var int
     *
     * @ORM\Column(name="cdn_status",type="integer", nullable=true)
     */
    protected $cdnStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cdn_flushed_at", type="datetime", nullable=true)
     */
    protected $cdnFlushedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    protected $galleryHasMedias;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
