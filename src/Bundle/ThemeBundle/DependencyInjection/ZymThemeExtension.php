<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ThemeBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpFoundation\RequestMatcher;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Zym\Bundle\ThemeBundle\Resolver\RequestMapResolver;
use Zym\Bundle\ThemeBundle\Resolver\ResolverInterface;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ZymThemeExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter($this->getAlias().'.themes', []);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $this->createThemeMap($config, $container);
    }

    private function createThemeMap($config, ContainerBuilder $container)
    {
        if (!$config['theme_map']) {
            return;
        }

        $this->addClassesToCompile([
            ResolverInterface::class,
        ]);

        foreach ($config['theme_map'] as $rule) {
            $matcher = $this->createRequestMatcher(
                $container,
                $rule['path'],
                $rule['host'],
                count($rule['methods']) === 0 ? null : $rule['methods'],
                $rule['ip']
            );

            $container->getDefinition(RequestMapResolver::class)
                      ->addMethodCall('add', [$matcher, $rule['theme']]);

            $parameterName = $this->getAlias().'.themes';
            $parameterValue = $container->getParameter($parameterName);
            $container->setParameter($parameterName, array_merge([$rule['theme']], $parameterValue));
        }
    }

    private function createRequestMatcher($container, $path = null, $host = null, $methods = null, $ip = null, array $attributes = [])
    {
        $serialized = serialize(array($path, $host, $methods, $ip, $attributes));
        $id = 'zym_theme.request_matcher.'.md5($serialized).sha1($serialized);

        if (isset($this->requestMatchers[$id])) {
            return $this->requestMatchers[$id];
        }

        // only add arguments that are necessary
        $arguments = array($path, $host, $methods, $ip, $attributes);
        while (count($arguments) > 0 && !end($arguments)) {
            array_pop($arguments);
        }

        $container
            ->register($id, RequestMatcher::class)
            ->setPublic(false)
            ->setArguments($arguments)
        ;

        return $this->requestMatchers[$id] = new Reference($id);
    }
}
