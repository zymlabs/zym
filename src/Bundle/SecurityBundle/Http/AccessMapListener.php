<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Http;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Zym\Security\Http\AccessMap;

class AccessMapListener
{
    /**
     * @var AccessMap
     */
    private $accessMap;

    /**
     * @var AccessRuleProviderInterface
     */
    private $accessRuleProvider;

    public function __construct(AccessMap $accessMap, AccessRuleProviderInterface $accessRuleProvider)
    {
        $this->accessMap = $accessMap;
        $this->accessRuleProvider = $accessRuleProvider;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        foreach ($this->accessRuleProvider->getRules() as $rule) {
            $this->accessMap->prepend($rule->getRequestMatcher(), $rule->getRoles(), $rule->getChannel());
        }
    }
}
