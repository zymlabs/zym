<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RuntimeConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use OpenSky\Bundle\RuntimeConfigBundle\Model\Parameter as BaseParameter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Acl\Model\DomainObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Zym\Bundle\RuntimeConfigBundle\Repository\ParameterRepository")
 * @ORM\Table(name="parameters")
 * @UniqueEntity(groups={"Entity"}, fields={"name"}, message="A parameter with the same name already exists; name must be unique")
 */
class Parameter extends BaseParameter implements DomainObjectInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @Assert\Length(groups={"Entity"}, min="0", max="255")
     * @Assert\NotBlank(groups={"Entity"})
     */
    protected $name;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Assert\Length(groups={"Entity"}, min="0", max="255")
     */
    protected $value = [];

    /**
     * {@inheritdoc}
     */
    public function getObjectIdentifier()
    {
        return md5($this->getName());
    }
}
