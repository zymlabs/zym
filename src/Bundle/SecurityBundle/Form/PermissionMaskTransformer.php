<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Form;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Transforms between a given value and a string.
 *
 * @author Joseph Bielawski <stloyd@gmail.com>
 */
class PermissionMaskTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        $values = [];

        $reflection = new \ReflectionClass(MaskBuilder::class);

        foreach ($reflection->getConstants() as $name => $cMask) {
            $cName = substr($name, 5);

            if (0 === strpos($name, 'MASK_')) {
                $maskValue = constant('Symfony\Component\Security\Acl\Permission\MaskBuilder::' . $name);

                if (($value & $maskValue) == $maskValue) {
                    $values[strtolower($cName)] = true;
                }
            }
        }

        return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        $maskBuilder = new MaskBuilder();

        foreach ($value as $mask => $maskValue) {
            if ($maskValue) {
                $maskBuilder->add($mask);
            }
        }

        return $maskBuilder->get();
    }
}
