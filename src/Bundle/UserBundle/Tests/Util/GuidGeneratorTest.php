<?php

namespace Zym\Bundle\UserBundle\Tests\Util;

use PHPUnit\Framework\TestCase;
use Zym\Bundle\UserBundle\Util\GuidGenerator;

class GuidGeneratorTest extends TestCase
{
    const GUID_PATTERN = '/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/';
    const GUID_v4_PATTERN = '/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/';

    public function testGenerateGuid()
    {
        $guids = [
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
        ];

        $this->assertRegExp(self::GUID_PATTERN, $guids[0]);
        $this->assertRegExp(self::GUID_PATTERN, $guids[1]);
        $this->assertRegExp(self::GUID_PATTERN, $guids[2]);
        $this->assertRegExp(self::GUID_PATTERN, $guids[3]);
        $this->assertRegExp(self::GUID_PATTERN, $guids[4]);
    }

    public function testGenerateGuidv4()
    {
        $guids = [
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
            GuidGenerator::generateGuid(),
        ];

        $this->assertRegExp(self::GUID_v4_PATTERN, $guids[0]);
        $this->assertRegExp(self::GUID_v4_PATTERN, $guids[1]);
        $this->assertRegExp(self::GUID_v4_PATTERN, $guids[2]);
        $this->assertRegExp(self::GUID_v4_PATTERN, $guids[3]);
        $this->assertRegExp(self::GUID_v4_PATTERN, $guids[4]);
    }
}
