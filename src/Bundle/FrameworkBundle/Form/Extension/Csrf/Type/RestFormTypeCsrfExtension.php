<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\FrameworkBundle\Form\Extension\Csrf\Type;

use Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension;
use Symfony\Component\Form\Util\ServerParams;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class RestFormTypeCsrfExtension extends FormTypeCsrfExtension
{
    public function __construct(
        CsrfTokenManagerInterface $defaultTokenManager,
        $defaultEnabled = true,
        $defaultFieldName = '_token',
        TranslatorInterface $translator = null,
        $translationDomain = null,
        ServerParams $serverParams = null,
        RequestStack $requestStack = null
    ) {
        // Check if we have a valid CSRF Token from RestFormCsrfSubscriber, if so turn off CSRF protection by default
        // This allows controllers to force their own CSRF protection for forms that require extra security.
        if ($requestStack) {
            $request = $requestStack->getCurrentRequest();
            if (!$request->isMethod(Request::METHOD_GET)
                && $request->attributes->has('_rest_csrf_valid')
                && $request->attributes->get('_rest_csrf_valid') == true) {
                $defaultEnabled = false;
            }
        }

        parent::__construct(
            $defaultTokenManager,
            filter_var($defaultEnabled, FILTER_VALIDATE_BOOLEAN),
            $defaultFieldName,
            $translator,
            $translationDomain
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return FormTypeCsrfExtension::class;
    }
}
