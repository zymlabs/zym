<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Form;

use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;

/**
 * Transforms between a given value and a string.
 *
 * @author Joseph Bielawski <stloyd@gmail.com>
 */
class AclSecurityIdentityToStringTransformer implements DataTransformerInterface
{
    private $choiceList;

    public function __construct(ChoiceListinterface $choiceList)
    {
        $this->choiceList = $choiceList;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        $roles = [];

        $entities = $this->choiceList->getChoices();

        foreach ($entities as $entity) {
            if (in_array($entity->getIdentifier(), (array)$value)) {
                $roles[] = (string)$entity->getId();
            }
        }

        return $roles;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        $roles = [];

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $entities = $this->choiceList->getChoicesForValues(array($value));

        if (count($value) !== count($entities)) {
            throw new TransformationFailedException('Not all entities matching the keys were found.');
        }

        foreach ($entities as $entity) {
            $roles = $entity->getIdentifier();
        }

        return $roles;
    }
}
