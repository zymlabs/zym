<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View as ViewResponse;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\UserBundle\Entity;
use Zym\Bundle\UserBundle\Entity\Group;
use Zym\Bundle\UserBundle\Form\DeleteType;
use Zym\Bundle\UserBundle\Form\GroupType;
use Zym\Bundle\UserBundle\Manager\GroupManager;

/**
 * Class GroupsController
 *
 * @package Zym\Bundle\UserBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class GroupsController extends Controller
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var GroupManager
     */
    private $groupManager;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, GroupManager $groupManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->groupManager = $groupManager;
    }

    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_user_groups",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     * @View()
     *
     * @ApiDoc(
     *     description="Returns a list user groups",
     *     section="User Groups",
     *     filters={
     *         {"name"="page", "dataType"="integer"},
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="orderBy", "dataType"="array"},
     *         {"name"="filterBy", "dataType"="array"}
     *     }
     * )
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy', []);
        $filterBy = $request->query->get('filterBy', []);

        $groups = $this->groupManager->findGroupsPaginated($filterBy, $page, $limit, $orderBy);

        return [
            'groups' => $groups,
        ];
    }

    /**
     * @Route(
     *     "/add.{_format}",
     *     name="zym_user_groups_add",
     *     defaults={"_format" = "html"},
     *     methods={"GET","POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     ".{_format}",
     *     name="zym_user_groups_post_add",
     *     methods={"POST"}
     * )
     * @View()
     * @ApiDoc(
     *     description="Add a group",
     *     section="User Groups",
     *     parameters={
     *         {"name"="name", "dataType"="string", "required"=true, "description"="Name", "readonly"=false},
     *         {"name"="roles", "dataType"="array", "required"=true, "description"="Roles", "readonly"=false}
     *     }
     * )
     * @param Request $request
     * @return ViewResponse|array
     */
    public function addAction(Request $request)
    {
        if (!$this->authorizationChecker->isGranted('CREATE', new ObjectIdentity('class', Group::class))) {
            throw new AccessDeniedException();
        }

        $group = new Group();
        $form = $this->createForm(GroupType::class, $group, [
            'method' => in_array($request->getMethod(), ['PUT', 'PATCH']) ? $request->getMethod() : 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->groupManager->updateGroup($group);

            return ViewResponse::createRouteRedirect(
                'zym_user_groups_show',
                ['id' => $group->getId()],
                Response::HTTP_CREATED
            );
        }

        return [
            'form' => $form,
        ];
    }

    /**
     * Show a group
     *
     * @param Entity\Group $group
     * @return array
     *
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_groups_show",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     *
     * @SecureParam(name="group", permissions="VIEW")
     *
     * @View()
     * @ApiDoc(
     *     description="Returns a group",
     *     section="User Groups"
     * )
     */
    public function showAction(Group $group)
    {
        return [
            'group' => $group,
        ];
    }

    /**
     * Edit a group
     *
     * @Route(
     *     "/{id}/edit.{_format}",
     *     name="zym_user_groups_edit",
     *     defaults={"_format" = "html"},
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_groups_put_edit",
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"PUT"}
     * )
     *
     * @SecureParam(name="group", permissions="EDIT")
     *
     * @View()
     * @ApiDoc(
     *     description="Edit a group",
     *     section="User Groups",
     *     parameters={
     *         {"name"="name", "dataType"="string", "required"=true, "description"="Name", "readonly"=false},
     *         {"name"="roles", "dataType"="array", "required"=true, "description"="Roles", "readonly"=false}
     *     }
     * )
     *
     * @param Request $request
     * @param Group   $group
     * @return ViewResponse|array
     */
    public function editAction(Request $request, Group $group)
    {
        $originalGroup = clone $group;
        $form          = $this->createForm(GroupType::class, $group);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->groupManager->updateGroup($group);

            return ViewResponse::createRouteRedirect(
                'zym_user_groups_show',
                ['id' => $group->getId()],
                Response::HTTP_OK
            );
        }

        return [
            'group' => $originalGroup,
            'form'  => $form->createView(),
        ];
    }

    /**
     * Delete a group
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_user_groups_delete",
     *     defaults={ "_format" = "html" },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_groups_delete_delete",
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"DELETE"}
     * )
     * @View()
     *
     * @SecureParam(name="group", permissions="DELETE")
     *
     * @ApiDoc(
     *     description="Delete a group",
     *     section="User Groups"
     * )
     *
     * @param Request $request
     * @param Group   $group
     * @return ViewResponse|array
     */
    public function deleteAction(Request $request, Group $group)
    {
        $form = $this->createForm(DeleteType::class, $group);
        $form->handleRequest($request);

        if ($form->isValid() || $request->isMethod(Request::METHOD_DELETE)) {
            $this->groupManager->deleteGroup($group);

            return ViewResponse::createRouteRedirect('zym_user_groups', [], Response::HTTP_OK);
        }

        return [
            'group' => $group,
            'form'  => $form,
        ];
    }
}
