<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\EntryInterface;
use Symfony\Component\Security\Acl\Model\MutableAclInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Entity\AclEntry;
use Zym\Bundle\SecurityBundle\Form\AclEntryType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;
use Zym\Bundle\SecurityBundle\Manager\AclClassManager;

/**
 * Acl Entries Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AclEntriesController extends Controller
{
    /**
     * @var AclClassManager
     */
    private $aclClassManager;

    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        AclClassManager $aclClassManager,
        AclProviderInterface $aclProvider,
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->aclClassManager = $aclClassManager;
        $this->aclProvider = $aclProvider;
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_security_acl_entries",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page = $request->query->get('page', 1);

        $aclClasses = $this->aclClassManager->findAclClasses([], $page);

        $oids = [];
        foreach ($aclClasses as $aclClass) {
            $oid = new ObjectIdentity('class', $aclClass->getClassType());

            try {
                $this->aclProvider->findAcl($oid);
            } catch (AclNotFoundException $e) {
                // Missing class level entry, add it
                $acl = $this->aclProvider->createAcl($oid);
                $this->aclProvider->updateAcl($acl);
                continue;
            }

            $oids[] = $oid;
        }

        $acls = $this->aclProvider->findAcls($oids);

        return [
            'aclClasses' => $aclClasses,
            'acls'       => $acls,
        ];
    }

    /**
     * @Route("/add/{classType}", name="zym_security_acl_entries_add")
     * @Template()
     *
     * @param Request  $request
     * @param AclClass $aclClass
     * @return array|RedirectResponse
     */
    public function addAction(Request $request, AclClass $aclClass)
    {
        if (!$this->authorizationChecker->isGranted('OPERATOR', new ObjectIdentity('class', $aclClass->getClassType()))) {
            throw new AccessDeniedException();
        }

        $oid = new ObjectIdentity('class', $aclClass->getClassType());

        /** @var MutableAclInterface|null $acl */
        $acl = $this->aclProvider->findAcl($oid);

        if (!$acl) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $classAce = new AclEntry();
        $form = $this->createForm(AclEntryType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->insertClassAce(
                    new RoleSecurityIdentity($classAce->getSecurityIdentity()),
                    $classAce->getMask(),
                    0,
                    $classAce->getMask(),
                    $classAce->getStrategy()
                );

                $this->aclProvider->updateAcl($acl);
                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'aclClass' => $aclClass,
            'form'     => $form->createView(),
        ];
    }

    /**
     * @Route("/{classType}/{index}/edit", name="zym_security_acl_entries_edit")
     * @Template()
     * @SecureParam(name="aclClass", permissions="OPERATOR")
     *
     * @param Request $request
     * @param AclClass $aclClass
     * @param int $index
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, AclClass $aclClass, $index)
    {
        if (!$this->authorizationChecker->isGranted('OPERATOR', new ObjectIdentity('class', $aclClass->getClassType()))) {
            throw new AccessDeniedException();
        }

        $origAclClass = clone $aclClass;
        $oid = new ObjectIdentity('class', $aclClass->getClassType());

        /** @var MutableAclInterface|null $acl */
        $acl = $this->aclProvider->findAcl($oid);

        $classAces = $acl->getClassAces();
        if (!isset($classAces[$index])) {
            throw $this->createNotFoundException('Index does not exist');
        }

        /** @var EntryInterface $classAce */
        $classAce = clone $classAces[$index];

        $form = $this->createForm(AclEntryType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->updateClassAce($index, $classAce->getMask());

                $this->aclProvider->updateAcl($acl);

                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'aclClass' => $origAclClass,
            'index'    => $index,
            'form'     => $form->createView(),
        ];
    }

    /**
     * Delete a aclClass
     *
     * @param AclClass $aclClass
     *
     * @Route(
     *     "/{classType}/{index}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{classType}/{index}/delete.{_format}",
     *     name="zym_security_acl_entries_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="aclClass", permissions="DELETE")
     *
     * @param Request $request
     * @param AclClass $aclClass
     * @param int $index
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request, AclClass $aclClass, $index)
    {
        $origAclClass = clone $aclClass;

        $oid = new ObjectIdentity('class', $aclClass->getClassType());

        /** @var MutableAclInterface $acl */
        $acl = $this->aclProvider->findAcl($oid);

        $classAces = $acl->getClassAces();

        if (!isset($classAces[$index])) {
            throw $this->createNotFoundException('Index does not exist');
        }

        /** @var EntryInterface $classAce */
        $classAce = clone $classAces[$index];

        $form = $this->createForm(DeleteType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->deleteClassAce($index);
                $this->aclProvider->updateAcl($acl);

                $this->addFlash('success', $this->translator->trans('Acl Entry Deleted'));

                $referer = $request->headers->get('referer');
                return $this->redirect($referer);
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $acl->deleteClassAce($index);
            $this->aclProvider->updateAcl($acl);

            return $this->redirectToRoute('zym_security_acl_entries');
        }

        return [
            'aclClass' => $origAclClass,
            'index'    => $index,
            'form'     => $form->createView(),
        ];
    }
}
