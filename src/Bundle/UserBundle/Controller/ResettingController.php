<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View as ViewResponse;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Zym\Bundle\UserBundle\Manager\UserManager;

/**
 * Class ResettingController
 *
 * @package Zym\Bundle\UserBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ResettingController extends Controller
{
    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var EngineInterface
     */
    private $engine;

    public function __construct(
        RouterInterface $router,
        TranslatorInterface $translator,
        MailerInterface $mailer,
        UserManager $userManager,
        EngineInterface $engine
    ) {
        $this->router = $router;
        $this->translator = $translator;
        $this->mailer = $mailer;
        $this->userManager = $userManager;
        $this->engine = $engine;
    }

    /**
     * Request reset user password: show form
     *
     * @Route(
     *     ".{_format}",
     *     name="zym_user_resetting_request",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     *  @Route(
     *     ".{_format}",
     *     name="fos_user_resetting_request",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @View()
     * @ApiDoc(
     *     description="Request reset user password.",
     *     section="User Resetting",
     *     parameters={
     *         {"name"="username", "dataType"="string", "required"=true, "description"="Username or Email address", "readonly"=false}
     *     }
     * )
     *
     * @param Request $request
     * @return ViewResponse|array
     */
    public function requestAction(Request $request)
    {
        $username = $request->request->get('username');

        if ($request->isMethod(Request::METHOD_POST)) {
            $user = $this->userManager->findUserByUsernameOrEmail($username);

            // Invalid username or email
            if (null === $user) {
                return ViewResponse::create([
                    'invalid_username' => $username,
                    'code' => Response::HTTP_PRECONDITION_FAILED,
                    'message' => $this->translator->trans('resetting.request.invalid_username', ['%username%' => $username], 'FOSUserBundle')
                ], Response::HTTP_PRECONDITION_FAILED);
            }

            // User already requested password
            if ($user->isPasswordRequestNonExpired($this->getParameter('fos_user.resetting.token_ttl'))) {
                return ViewResponse::create([
                    'code' => Response::HTTP_CONFLICT,
                    'message' => $this->translator->trans('resetting.password_already_requested', ['%username%' => $username], 'FOSUserBundle')
                ], Response::HTTP_CONFLICT);
            }

            // Generate confirmation token
            if (null === $user->getConfirmationToken()) {
                /** @var TokenGeneratorInterface $tokenGenerator */
                $tokenGenerator = $this->get('fos_user.util.token_generator');
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }

            $request->getSession()->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
            $this->mailer->sendResettingEmailMessage($user);

            $user->setPasswordRequestedAt(new \DateTime());
            $this->userManager->updateUser($user);

            // Issue with session not being saved completely before the redirect happens.
            $request->getSession()->save();

            return ViewResponse::createRouteRedirect('zym_user_resetting_check_email', [
                '_format' => $request->getRequestFormat(),
            ]);
        }

        return [];
    }

    /**
     * Request reset user password: submit form and send email
     *
     * @Route(
     *     "send-email.{_format}",
     *     name="zym_user_resetting_send_email",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"POST"},
     *     options={"expose" = true}
     * )
     * @View()
     * @ApiDoc(
     *     description="Request reset user password: submit form and send email",
     *     section="User Resetting",
     *     parameters={
     *         {"name"="username", "dataType"="string", "required"=true, "description"="Username or Email address", "readonly"=false}
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');

        $user = $this->userManager->findUserByUsernameOrEmail($username);

        if (null === $user) {
            return $this->engine->renderResponse('FOSUserBundle:Resetting:request.html.' . $this->getEngine(), ['invalid_username' => $username]);
        }

        if ($user->isPasswordRequestNonExpired($this->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->get('templating')->renderResponse('FOSUserBundle:Resetting:passwordAlreadyRequested.html.' . $this->getEngine());
        }

        if (null === $user->getConfirmationToken()) {
            /** @var TokenGeneratorInterface $tokenGenerator */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $request->getSession()->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->mailer->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->userManager->updateUser($user);

        // Issue with session not being saved completely before the redirect happens.
        $request->getSession()->save();

        return $this->redirectToRoute('fos_user_resetting_check_email');
    }

    /**
     * Tell the user to check his email provider
     *
     *  @Route(
     *     "check-email.{_format}",
     *     name="zym_user_resetting_check_email",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     "check-email.{_format}",
     *     name="fos_user_resetting_check_email",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     * @View()
     *
     * @param Request $request
     * @return Response|array
     */
    public function checkEmailAction(Request $request)
    {
        $session = $request->getSession();
        $email = $session->get(static::SESSION_EMAIL);

        if (empty($email)) {
            // the user does not come from the sendEmail action
            return $this->redirectToRoute('fos_user_resetting_request');
        }

        return [
            'email' => $email,
        ];
    }

    /**
     * Reset user password
     *
     * @Route(
     *     "reset/{token}.{_format}",
     *     name="zym_user_resetting_reset",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     "reset/{token}.{_format}",
     *     name="fos_user_resetting_reset",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @View()
     * @ApiDoc(
     *     description="Reset user password.",
     *     section="User Resetting",
     *     parameters={
     *         {"name"="fos_user_resetting_form[plainPassword][first]", "dataType"="string", "required"=true, "description"="Password", "readonly"=false}
     *     }
     * )
     */
    public function resetAction(Request $request, $token)
    {
        /** @var \FOS\UserBundle\Form\Factory\FactoryInterface $formFactory */
        $formFactory = $this->get('fos_user.resetting.form.factory');
        /** @var \FOS\UserBundle\Model\UserManagerInterface $userManager */
        $userManager = $this->get('fos_user.user_manager');
        /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->get('router')->generate('fos_user_security_login');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                return $response;
            }
        }

        return [
            'token' => $token,
            'form'  => $form,
        ];
    }

    /**
     * Get the truncated email displayed when requesting the resetting.
     *
     * The default implementation only keeps the part following @ in the address.
     *
     * @param UserInterface $user
     *
     * @return string
     */
    protected function getObfuscatedEmail(UserInterface $user)
    {
        $email = $user->getEmail();

        if (false !== $pos = strpos($email, '@')) {
            $email = '...' . substr($email, $pos);
        }

        return $email;
    }
}
