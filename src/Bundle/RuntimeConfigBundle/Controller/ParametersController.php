<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
  */

namespace Zym\Bundle\RuntimeConfigBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Zym\Bundle\RuntimeConfigBundle\Entity\Parameter;
use Zym\Bundle\RuntimeConfigBundle\Form\DeleteType;
use Zym\Bundle\RuntimeConfigBundle\Form\ParameterType;
use Zym\Bundle\RuntimeConfigBundle\Manager\ParameterManager;

/**
 * Runtime Config Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class ParametersController extends Controller
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var ParameterManager
     */
    private $parameterManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        FormFactoryInterface $formFactory,
        ParameterManager $parameterManager,
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->formFactory = $formFactory;
        $this->parameterManager = $parameterManager;
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="zym_runtime_config_parameters")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $parameters = $this->parameterManager->findParameters($filterBy, $page, $limit, $orderBy);

        return [
            'parameters' => $parameters,
        ];
    }

    /**
     * Show a parameter
     *
     * @Route(
     *     "/{name}/view.{_format}",
     *     name="zym_runtime_config_parameters_show",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+",
     *         "_format" = "html|json"
     *     }
     * )
     * @ParamConverter("parameter", class="ZymRuntimeConfigBundle:Parameter")
     * @Template()
     *
     * @SecureParam(name="parameter", permissions="VIEW")
     *
     * @param Parameter $parameter
     * @return array
     */
    public function showAction(Parameter $parameter)
    {
        return [
            'parameter' => $parameter,
        ];
    }

    /**
     * @Route(
     *     "/add.{_format}",
     *     name="zym_runtime_config_parameters_add",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        if (!$this->authorizationChecker->isGranted('CREATE', new ObjectIdentity('class', Parameter::class))) {
            throw new AccessDeniedException();
        }

        $parameter = new Parameter();

        $form = $this->formFactory->create(ParameterType::class, $parameter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->parameterManager->createParameter($parameter);

                $this->addFlash('success', $this->translator->trans('Added successfully!'));

                return $this->redirectToRoute('zym_runtime_config_parameters');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{name}/edit", name="zym_runtime_config_parameters_edit")
     * @ParamConverter("parameter", class="ZymRuntimeConfigBundle:Parameter")
     * @Template()
     *
     * @SecureParam(name="parameter", permissions="EDIT")
     *
     * @param Request $request
     * @param Parameter $parameter
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Parameter $parameter)
    {
        $origParameter = clone $parameter;
        $form = $this->formFactory->create(ParameterType::class, $parameter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->parameterManager->saveParameter($parameter);

                $this->addFlash('success', $this->translator->trans('Changes saved!'));

                return $this->redirectToRoute('zym_runtime_config_parameters');
            }
        }

        return [
            'parameter' => $origParameter,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a parameter
     *
     * @param Parameter $parameter
     *
     * @Route(
     *     "/{name}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     * @Route(
     *     "/{name}/delete.{_format}",
     *     name="zym_runtime_config_parameters_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="parameter", permissions="DELETE")
     *
     * @param Request $request
     * @param Parameter $parameter
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request, Parameter $parameter)
    {
        $origParameter = clone $parameter;

        $form = $this->formFactory->create(DeleteType::class, $parameter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->parameterManager->deleteParameter($parameter);

                $this->addFlash('success', $this->translator->trans('Parameter Deleted'));

                return $this->redirectToRoute('zym_runtime_config_parameters');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $this->parameterManager->deleteParameter($parameter);

            return $this->redirectToRoute('zym_runtime_config_parameters');
        }

        return [
            'parameter' => $origParameter,
            'form' => $form->createView(),
        ];
    }
}
