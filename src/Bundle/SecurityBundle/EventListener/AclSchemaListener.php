<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Zym\Bundle\SecurityBundle\EventListener;

use Doctrine\ORM\Tools\Event\GenerateSchemaTableEventArgs;
use Symfony\Bundle\AclBundle\EventListener\AclSchemaListener as BaseAclSchemaListener;
use Symfony\Component\Security\Acl\Dbal\Schema;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Entity\AclEntry;
use Zym\Bundle\SecurityBundle\Entity\AclObjectIdentity;
use Zym\Bundle\SecurityBundle\Entity\AclObjectIdentityAncestor;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;

class AclSchemaListener extends BaseAclSchemaListener
{
    private $schema;

    public function __construct(Schema $schema)
    {
        parent::__construct($schema);

        $this->schema = $schema;
    }

    public function postGenerateSchemaTable(GenerateSchemaTableEventArgs $args)
    {
        switch ($args->getClassMetadata()->getName()) {
            case AclClass::class:
            case AclEntry::class:
            case AclObjectIdentity::class:
            case AclObjectIdentityAncestor::class:
            case AclSecurityIdentity::class:
                $tableName = $args->getClassTable()->getName();
                $schema = $args->getSchema();
                $schema->dropTable($tableName);
                break;

            default:
        }
    }
}
