<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Manager;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\SecurityBundle\Entity\AccessRule;
use Zym\Bundle\SecurityBundle\Http\AccessRuleInterface;
use Zym\Bundle\SecurityBundle\Http\AccessRuleProviderInterface;
use Zym\Bundle\SecurityBundle\Repository\AccessRuleRepository;

/**
 * Access Rule Manager
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class AccessRuleManager extends AbstractEntityManager implements AccessRuleProviderInterface
{
    /**
     * @var AccessRuleRepository
     */
    protected $repository;

    /**
     * @param AccessRule $accessRule
     * @return AccessRule
     */
    public function createAccessRule(AccessRule $accessRule)
    {
        return $this->createEntity($accessRule);
    }

    /**
     * @param AccessRule $accessRule
     */
    public function deleteAccessRule(AccessRule $accessRule)
    {
        $this->deleteEntity($accessRule);
    }

    /**
     * @param AccessRule $accessRule
     * @param bool $andFlush
     * @return AccessRule
     */
    public function saveAccessRule(AccessRule $accessRule, bool $andFlush = true)
    {
        return $this->saveEntity($accessRule, $andFlush);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findAccessRules(array $criteria = [], int $page = 1, int $limit = 50, array $orderBy = [])
    {
        return $this->repository->findAccessRules($criteria, $page, $limit, $orderBy);
    }

    /**
     * @param array $criteria
     * @return AccessRuleInterface|null
     */
    public function findAccessRuleBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @return AccessRuleInterface[]
     */
    public function getRules()
    {
        return $this->repository->findAll();
    }
}
