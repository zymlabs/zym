<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;
use Zym\Bundle\SecurityBundle\Form\AclSecurityIdentityType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;
use Zym\Bundle\SecurityBundle\Manager\AclSecurityIdentityManager;

/**
 * Acl Roles Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AclRolesController extends Controller
{
    /**
     * @var AclSecurityIdentityManager
     */
    private $aclSecurityIdentityManager;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        AclSecurityIdentityManager $aclSecurityIdentityManager,
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->aclSecurityIdentityManager = $aclSecurityIdentityManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_security_acl_roles",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = array_merge(['username' => false], (array) $request->query->get('filterBy', []));

        $roles = $this->aclSecurityIdentityManager->findAclSecurityIdentities($filterBy, $page, $limit, $orderBy);

        return [
            'roles' => $roles,
        ];
    }

    /**
     * @Route("/add", name="zym_security_acl_roles_add")
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        if (!$this->authorizationChecker->isGranted('CREATE', new ObjectIdentity('class', AclSecurityIdentity::class))) {
            throw new AccessDeniedException();
        }

        $role = new AclSecurityIdentity();
        $form = $this->createForm(AclSecurityIdentityType::class, $role);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->aclSecurityIdentityManager->createAclSecurityIdentity($role);

                $this->addFlash('success', $this->translator->trans('Created the new role successfully.'));

                return $this->redirectToRoute('zym_security_acl_roles');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/edit", name="zym_security_acl_roles_edit")
     * @Template()
     *
     * @SecureParam(name="role", permissions="EDIT")
     *
     * @param Request $request
     * @param AclSecurityIdentity $role
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, AclSecurityIdentity $role)
    {
        $origNode = clone $role;

        $form = $this->createForm(AclSecurityIdentityType::class, $role);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->aclSecurityIdentityManager->saveAclSecurityIdentity($role);

                $this->addFlash('success', $this->translator->trans('Changes saved!'));

                return $this->redirectToRoute('backend_roles');
            }
        }

        return [
            'role' => $origNode,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a role
     *
     * @param AclSecurityIdentity $role
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_security_acl_roles_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="role", permissions="DELETE")
     *
     * @param Request $request
     * @param AclSecurityIdentity $role
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request, AclSecurityIdentity $role)
    {
        $origNode = clone $role;

        $form = $this->createForm(DeleteType::class, $role);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->aclSecurityIdentityManager->deleteAclSecurityIdentity($role);

                $this->addFlash('success', $this->translator->trans('Role Deleted'));

                return $this->redirectToRoute('zym_security_acl_roles');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $this->aclSecurityIdentityManager->deleteAclSecurityIdentity($role);

            return $this->redirectToRoute('zym_security_acl_roles');
        }

        return [
            'role' => $origNode,
            'form' => $form->createView(),
        ];
    }
}
