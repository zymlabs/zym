<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle;

use JMS\Serializer\Annotation as Serializer;
use Zym\Bundle\ResqueBundle\Failure\Redis;

/**
 *
 * @Serializer\ExclusionPolicy("all")
 */
abstract class AbstractJob extends \Resque_Job
{
    /**
     * @var \Resque_Job
     *
     * @Serializer\Exclude
     */
    public $job;

    /**
     * @var string The queue name
     *
     * @Serializer\Expose
     */
    public $queue = 'default';

    /**
     * @var \Resque_Worker Instance of the Resque worker running this job.
     *
     * @Serializer\Exclude
     */
    public $worker;

    /**
     * @var array The job args
     *
     * @Serializer\Expose
     */
    public $args = [];

    /**
     * Instantiate a new instance of a job.
     */
    public function __construct()
    {
        parent::__construct($this->queue, []);
    }

    /**
     * Get the job name
     *
     * @return string
     *
     * @Serializer\VirtualProperty
     */
    public function getName()
    {
        return \get_class($this);
    }

    /**
     * Get the time the job was queued
     *
     * @return string
     *
     * @Serializer\VirtualProperty
     */
    public function getQueuedAt()
    {
        if (isset($this->job) && $this->job->payload) {
            list($usec, $sec) = explode('.', $this->job->payload['queue_time']); //split the microtime on space

            return new \DateTime($sec);
        }

        return null;
    }

    /**
     * Setup the job
     *
     * @return void
     */
    public function setUp()
    {
    }

    /**
     * Perform the job
     *
     * @return void
     */
    public function perform()
    {
        \Resque_Failure::setBackend(Redis::class);
        $this->run($this->args);
    }

    /**
     * Run
     *
     * @return void
     */
    public abstract function run(array $args);

    /**
     * Tear down
     *
     * @return void
     */
    public function tearDown()
    {
    }
}
