<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Manager;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;
use Zym\Bundle\SecurityBundle\Repository\AclSecurityIdentityRepository;

/**
 * Acl Security Identity Manager
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class AclSecurityIdentityManager extends AbstractEntityManager
{
    /**
     * @var AclSecurityIdentityRepository
     */
    protected $repository;

    /**
     * @param AclSecurityIdentity $aclSecurityIdentity
     * @return AclSecurityIdentity
     */
    public function createAclSecurityIdentity(AclSecurityIdentity $aclSecurityIdentity)
    {
        return $this->createEntity($aclSecurityIdentity);
    }

    /**
     * @param AclSecurityIdentity $aclSecurityIdentity
     */
    public function deleteAclSecurityIdentity(AclSecurityIdentity $aclSecurityIdentity)
    {
        $this->deleteEntity($aclSecurityIdentity);
    }

    /**
     * @param AclSecurityIdentity $aclSecurityIdentity
     * @param bool $andFlush
     * @return AclSecurityIdentity
     */
    public function saveAclSecurityIdentity(AclSecurityIdentity $aclSecurityIdentity, bool $andFlush = true)
    {
        return $this->saveEntity($aclSecurityIdentity, $andFlush);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findAclSecurityIdentities(array $criteria = [], int $page = 1, int $limit = 50, array $orderBy = [])
    {
        return $this->repository->findAclSecurityIdentities($criteria, $page, $limit, $orderBy);
    }

    /**
     * @param array $criteria
     * @return AclSecurityIdentity|null
     */
    public function findAclSecurityIdentityBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }
}
