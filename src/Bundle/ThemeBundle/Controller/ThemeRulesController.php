<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ThemeBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Zym\Bundle\ThemeBundle\Entity\ThemeRule;
use Zym\Bundle\ThemeBundle\Form\DeleteType;
use Zym\Bundle\ThemeBundle\Form\ThemeRuleType;
use Zym\Bundle\ThemeBundle\Manager\ThemeRuleManager;

/**
 * Class ThemeRulesController
 *
 * @package Zym\Bundle\ThemeBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ThemeRulesController extends Controller
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var ThemeRuleManager
     */
    private $themeRuleManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        ThemeRuleManager $themeRuleManager,
        TranslatorInterface $translator
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->themeRuleManager = $themeRuleManager;
        $this->translator = $translator;
    }

    /**
     * @Route("", name="zym_theme_theme_rules")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy', []);
        $filterBy = $request->query->get('filterBy', []);

        $themeRules = $this->themeRuleManager->findThemeRules($filterBy, $page, $limit, $orderBy);

        return [
            'themeRules' => $themeRules,
        ];
    }

    /**
     * @Route("/add", name="zym_theme_theme_rules_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        if (!$this->authorizationChecker->isGranted('OPERATOR', new ObjectIdentity('class', ThemeRule::class))) {
            throw new AccessDeniedException();
        }

        $themeRule = new ThemeRule();
        $form = $this->createForm(ThemeRuleType::class, $themeRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->themeRuleManager->createThemeRule($themeRule);

                $translator = $this->get('translator');

                $this->addFlash('success', $translator->trans('Created the new theme rule successfully.'));

                return $this->redirectToRoute('zym_theme_theme_rules');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/edit", name="zym_theme_theme_rules_edit")
     * @Template()
     * @SecureParam(name="themeRule", permissions="EDIT")
     */
    public function editAction(Request $request, ThemeRule $themeRule)
    {
        $origThemeRule = clone $themeRule;
        $form = $this->createForm(ThemeRuleType::class, $themeRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->themeRuleManager->saveThemeRule($themeRule);

                $this->addFlash('success', $this->translator->trans('Changes saved!'));

                return $this->redirectToRoute('zym_theme_theme_rules');
            }
        }

        return [
            'themeRule' => $origThemeRule,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Delete an theme rule
     *
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_theme_theme_rules_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="themeRule", permissions="DELETE")
     */
    public function deleteAction(Request $request, ThemeRule $themeRule)
    {
        $origThemeRule = clone $themeRule;
        $form = $this->createForm(DeleteType::class, $themeRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->themeRuleManager->deleteThemeRule($themeRule);

                $this->addFlash('success', $this->translator->trans('Theme rule deleted.'));

                return $this->redirectToRoute('zym_theme_theme_rules');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $this->themeRuleManager->deleteThemeRule($themeRule);
            return $this->redirectToRoute('zym_theme_theme_rules');
        }

        return [
            'themeRule' => $origThemeRule,
            'form'      => $form->createView(),
        ];
    }
}
