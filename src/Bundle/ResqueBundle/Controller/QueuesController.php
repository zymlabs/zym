<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Zym\Bundle\ResqueBundle\Resque;

class QueuesController extends Controller
{
    /**
     * @var Resque
     */
    private $resque;

    public function __construct(Resque $resque)
    {
        $this->resque = $resque;
    }

    /**
     * @Route(
     *    ".{_format}",
     *    name="zym_resque_queues",
     *    defaults={ "_format" = "html" },
     *    options={ "expose"="true" }
     * )
     * @View()
     */
    public function indexAction()
    {
        $queues = $this->resque->getQueues();

        return [
            'queues' => $queues,
        ];
    }

    /**
     * @Route(
     *    "{queue}/jobs.{_format}",
     *    name="zym_resque_queues_jobs",
     *    defaults={ "_format" = "html" },
     *    options={ "expose"="true" }
     * )
     * @View()
     */
    public function jobsAction(Request $request)
    {
        $queueName = $request->get('queue');

        $page     = (int)$request->query->get('page', 1);
        $limit    = (int)$request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $queue = $this->resque->getQueue($queueName);

        $count = $queue->countJobs();
        $start = $count - ($page * $limit);
        $jobs  = $queue->getJobs($start < 0 ? 0 : $start, $start < 0 ? $limit - abs($start) : $limit);

        return [
            'jobs' => [
                'pageIndex'    => (int)$page,
                'itemsPerPage' => (int)$limit,
                'items'        => $jobs,
                'totalItems'   => (int)$count,
            ],
        ];
    }
}
