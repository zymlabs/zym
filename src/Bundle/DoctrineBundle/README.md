Zym Doctrine Bundle
===================

This bundle provides the following functionality:
    - "json" column type for storing data as JSON
in the database.
    - "datetime" column type stored as UTC.
    - "date" column type stored as UTC.
