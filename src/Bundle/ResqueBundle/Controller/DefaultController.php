<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zym\Bundle\ResqueBundle\Resque;

class DefaultController extends Controller
{
    /**
     * @var Resque
     */
    private $resque;

    public function __construct(Resque $resque)
    {
        $this->resque = $resque;
    }

    /**
     * @Route(
     *    ".{_format}",
     *    name="zym_resque",
     *    defaults={ "_format" = "html" },
     *    options={ "expose"="true" }
     * )
     * @View()
     */
    public function indexAction()
    {
//        $a = new ExceptionJob();
//        $this->resque->enqueueOnce($a);

        return [
            'resque'  => $this->resque,
        ];
    }
}
