<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Zym\Bundle\ResqueBundle\Failure\Redis as FailedJobs;
use Zym\Bundle\ResqueBundle\Resque;

class FailuresController extends Controller
{
    /**
     * @var Resque
     */
    private $resque;

    public function __construct(Resque $resque)
    {
        $this->resque = $resque;
    }

    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_resque_failures",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     options={ "expose" = "true" }
     * )
     * @View()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $start = FailedJobs::count() - ($page * $limit);
        $failures = array_reverse(FailedJobs::all($start < 0 ? 0 : $start, $start < 0 ? $limit - abs($start) : $limit));

        return [
            'resque'   => $this->resque,
            'failures' => $failures,
            'page'     => (int)$page,
            'limit'    => (int)$limit,
            'count'    => FailedJobs::count(),
        ];
    }
}
