<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Acl\Domain\AclCollectionCache;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Exception\NotAllAclsFoundException;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\UserBundle\Repository\UserRepository;

/**
 * Class UserManager
 *
 * @package Zym\Bundle\UserBundle\Entity
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class UserManager extends AbstractEntityManager implements UserManagerInterface
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var PasswordUpdaterInterface
     */
    private $passwordUpdater;

    /**
     * @var CanonicalFieldsUpdater
     */
    private $canonicalFieldsUpdater;

    public function __construct(
        EntityManagerInterface $entityManager,
        string $class,
        PaginatorInterface $paginator,
        AclProviderInterface $aclProvider,
        TokenStorageInterface $tokenStorage = null,
        AclCollectionCache $aclCollectionCache = null,
        PasswordUpdaterInterface $passwordUpdater,
        CanonicalFieldsUpdater $canonicalFieldsUpdater
    ) {
        parent::__construct($entityManager, $class, $paginator, $aclProvider, $tokenStorage, $aclCollectionCache);

        $this->passwordUpdater = $passwordUpdater;
        $this->canonicalFieldsUpdater = $canonicalFieldsUpdater;
    }

    /**
     * {@inheritdoc}
     */
    public function createUser()
    {
        $class = $this->getClass();
        $user = new $class();

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByEmail($email)
    {
        return $this->findUserBy(['emailCanonical' => $this->canonicalFieldsUpdater->canonicalizeEmail($email)]);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByUsername($username)
    {
        return $this->findUserBy(['usernameCanonical' => $this->canonicalFieldsUpdater->canonicalizeUsername($username)]);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        if (preg_match('/^.+\@\S+\.\S+$/', $usernameOrEmail)) {
            $user = $this->findUserByEmail($usernameOrEmail);
            if (null !== $user) {
                return $user;
            }
        }

        return $this->findUserByUsername($usernameOrEmail);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByConfirmationToken($token)
    {
        return $this->findUserBy(['confirmationToken' => $token]);
    }

    /**
     * {@inheritdoc}
     */
    public function updateCanonicalFields(UserInterface $user)
    {
        $this->canonicalFieldsUpdater->updateCanonicalFields($user);
    }

    /**
     * {@inheritdoc}
     */
    public function updatePassword(UserInterface $user)
    {
        $this->passwordUpdater->hashPassword($user);
    }

    /**
     * @return PasswordUpdaterInterface
     */
    protected function getPasswordUpdater()
    {
        return $this->passwordUpdater;
    }

    /**
     * @return CanonicalFieldsUpdater
     */
    protected function getCanonicalFieldsUpdater()
    {
        return $this->canonicalFieldsUpdater;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteUser(UserInterface $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        if (false !== strpos($this->class, ':')) {
            $metadata = $this->entityManager->getClassMetadata($this->class);
            $this->class = $metadata->getName();
        }

        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function findUserBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findUsers()
    {
        return $this->repository->findAll();
    }

    /**
     * Find users
     *
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginatorInterface
     */
    public function findUsersPaginated(array $criteria = null, int $page = 1, int $limit = 50, array $orderBy = null)
    {
        $entities = $this->repository->findUsers($criteria, $page, $limit, $orderBy);
        $this->loadAcls($entities);

        return $entities;
    }

    /**
     * {@inheritdoc}
     */
    public function reloadUser(UserInterface $user)
    {
        $this->entityManager->refresh($user);
    }

    /**
     * {@inheritdoc}
     */
    public function updateUser(UserInterface $user, bool $andFlush = true)
    {
        $this->updateCanonicalFields($user);
        $this->updatePassword($user);

        if ($this->entityManager->getUnitOfWork()->getEntityState($user) === UnitOfWork::STATE_NEW) {
            $this->createEntity($user);
        } else {
            $this->saveEntity($user, $andFlush);
        }
    }

    /**
     * Create an entity
     *
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function createEntity($entity)
    {
        $this->entityManager->beginTransaction();

        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            $oid = ObjectIdentity::fromDomainObject($entity);

            try {
                $acl = $this->aclProvider->createAcl($oid);

                // Users shouldn't be able to change there own roles
                $builder = new MaskBuilder();
                $builder->add('view')
                    ->add('edit');

                $mask = $builder->get();
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($entity), $mask);

                $builder = new MaskBuilder();
                $builder->add('delete');

                $mask = $builder->get();
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($entity), $mask, 0, false);

                $this->aclProvider->updateAcl($acl);
            } catch (AclAlreadyExistsException $e) {

            }

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Delete an entity
     *
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function deleteEntity($entity)
    {
        $this->entityManager->beginTransaction();

        try {
            $oid = ObjectIdentity::fromDomainObject($entity);
            $this->aclProvider->deleteAcl($oid);

            $this->entityManager->remove($entity);
            $this->entityManager->flush();

            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Save an entity
     *
     * @param object $entity
     * @param bool   $andFlush
     * @return object|null
     */
    protected function saveEntity($entity, bool $andFlush = true)
    {
        if ($this->entityManager->getUnitOfWork()->getEntityState($entity) === UnitOfWork::STATE_NEW) {
            return $this->createEntity($entity);
        } else {
            if (method_exists($entity, 'setUpdatedAt')) {
                $entity->setUpdatedAt(new \DateTime());
            }

            $this->entityManager->persist($entity);

            if ($andFlush) {
                $this->entityManager->flush();
            }
        }
    }

    /**
     * Preload acls for entities
     *
     * @param array $entities
     */
    protected function loadAcls($entities)
    {
        $aclCollectionCache = $this->getAclCollectionCache();

        try {
            if ($aclCollectionCache) {
                $sortedEntities = [];
                foreach ($entities as $entity) {
                    $sortedEntities[get_class($entity)][] = $entity;
                }
                foreach ($sortedEntities as $entitiesGroup) {
                    $aclCollectionCache->cache($entitiesGroup);
                }
            }
        } catch (NotAllAclsFoundException $e) {
            // At least we tried...
        } catch (AclNotFoundException $e) {
            // At least we tried...
        }
    }
}
