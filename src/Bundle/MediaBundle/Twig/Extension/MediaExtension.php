<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\MediaBundle\Twig\Extension;

use Zym\Bundle\MediaBundle\MediaPool;
use Zym\Bundle\MediaBundle\Model\MediaInterface;
use Zym\Bundle\MediaBundle\Model\MediaManagerInterface;

class MediaExtension extends \Twig_Extension
{
    /**
     * @var MediaPool
     */
    private $mediaPool;

    /**
     * @var MediaManagerInterface
     */
    private $mediaManager;

    /**
     * @var array
     */
    private $resources = [];

    public function __construct(MediaPool $mediaPool, MediaManagerInterface $mediaManager)
    {
        $this->mediaPool    = $mediaPool;
        $this->mediaManager = $mediaManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('media', [$this, 'media'], [
                'is_safe' => ['html'],
                'needs_environment' => true,
            ]),
            new \Twig_SimpleFunction('media_thumbnail', [$this, 'thumbnail'], [
                'is_safe' => ['html'],
                'needs_environment' => true,
            ]),
            new \Twig_SimpleFunction('media_path', [$this, 'path']),
        ];
    }

    /**
     * @param mixed $media
     * @return MediaInterface|null
     */
    private function getMedia($media)
    {
        if (!$media instanceof MediaInterface && strlen($media) > 0) {
            $media = $this->mediaManager->findOneBy([
                'id' => $media,
            ]);
        }

        if (!$media instanceof MediaInterface) {
            return false;
        }

        if ($media->getProviderStatus() !== MediaInterface::STATUS_OK) {
            return false;
        }

        return $media;
    }

    /**
     * @param \Twig_Environment $environment
     * @param string            $template
     * @param array             $parameters
     *
     * @return mixed
     */
    public function render(\Twig_Environment $environment, $template, array $parameters = [])
    {
        if (!isset($this->resources[$template])) {
            $this->resources[$template] = $environment->loadTemplate($template);
        }

        return $this->resources[$template]->render($parameters);
    }

    /**
     * @param \Twig_Environment                        $environment
     * @param \Sonata\MediaBundle\Model\MediaInterface $media
     * @param string                                   $format
     * @param array                                    $options
     *
     * @return string
     */
    public function media(\Twig_Environment $environment, $media = null, $format = 'reference', $options = [])
    {
        $media = $this->getMedia($media);

        if (!$media) {
            return '';
        }

        $provider = $this->mediaPool->getProvider($media->getProviderName());

        $format  = $provider->getFormatName($media, $format);
        $options = $provider->getHelperProperties($media, $format, $options);

        return $this->render($environment, $provider->getTemplate('view'), [
            'media' => $media,
            'format' => $format,
            'options' => $options,
        ]);
    }

    /**
     * @param \Sonata\MediaBundle\Model\MediaInterface $media
     * @param string                                   $format
     *
     * @return string
     */
    public function path($media = null, $format)
    {
        $media = $this->getMedia($media);

        if (!$media) {
             return '';
        }

        $provider = $this->mediaPool->getProvider($media->getProviderName());
        $format = $provider->getFormatName($media, $format);

        return $provider->generatePublicUrl($media, $format);
    }

    /**
     * Returns the thumbnail for the provided media
     *
     * @param \Twig_Environment                        $environment
     * @param \Sonata\MediaBundle\Model\MediaInterface $media
     * @param string                                   $format
     * @param array                                    $options
     *
     * @return string
     */
    public function thumbnail(\Twig_Environment $environment, $media = null, $format, $options = [])
    {
        $media = $this->getMedia($media);

        if (!$media) {
            return '';
        }

        $provider = $this->mediaPool->getProvider($media->getProviderName());

        $format = $provider->getFormatName($media, $format);
        $formatDefinition = $provider->getFormat($format);

        // build option
        $defaultOptions = [
            'title' => $media->getName(),
        ];

        if ($formatDefinition['width']) {
            $defaultOptions['width'] = $formatDefinition['width'];
        }
        if ($formatDefinition['height']) {
            $defaultOptions['height'] = $formatDefinition['height'];
        }

        $options = array_merge($defaultOptions, $options);

        $options['src'] = $provider->generatePublicUrl($media, $format);

        return $this->render($environment, $provider->getTemplate('thumbnail'), [
            'media' => $media,
            'options' => $options,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'zym_media';
    }
}
