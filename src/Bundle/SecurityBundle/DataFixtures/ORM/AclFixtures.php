<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Zym\Bundle\SecurityBundle\Entity\AccessRule;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Entity\AclEntry;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;

class AclFixtures extends Fixture
{
    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    public function __construct(AclProviderInterface $aclProvider)
    {
        $this->aclProvider = $aclProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        // Roles

        $identity = new AclSecurityIdentity();
        $identity->setIdentifier('IS_AUTHENTICATED_ANONYMOUSLY');
        $identity->setUsername(false);
        $manager->persist($identity);

        $identity = new AclSecurityIdentity();
        $identity->setIdentifier('IS_AUTHENTICATED_FULLY');
        $identity->setUsername(false);
        $manager->persist($identity);

        $identity = new AclSecurityIdentity();
        $identity->setIdentifier('IS_AUTHENTICATED_REMEMBERED');
        $identity->setUsername(false);
        $manager->persist($identity);

        $identity = new AclSecurityIdentity();
        $identity->setIdentifier('ROLE_ALLOWED_TO_SWITCH');
        $identity->setUsername(false);
        $manager->persist($identity);

        $identity = new AclSecurityIdentity();
        $identity->setIdentifier('ROLE_USER');
        $identity->setUsername(false);
        $manager->persist($identity);

        // AclClass
        try {
            $oid = new ObjectIdentity('class', AclClass::class);
            $acl = $this->aclProvider->createAcl($oid);
        } catch (AclAlreadyExistsException $e) {
            $acl = $this->aclProvider->findAcl($oid);
        }

        $sid = new RoleSecurityIdentity('ROLE_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_VIEW);

        // insert ACEs for the super admin
        $sid = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_IDDQD);
        $this->aclProvider->updateAcl($acl);

        // AclEntry
        try {
            $oid = new ObjectIdentity('class', AclEntry::class);
            $acl = $this->aclProvider->createAcl($oid);
        } catch (AclAlreadyExistsException $e) {
            $acl = $this->aclProvider->findAcl($oid);
        }

        $sid = new RoleSecurityIdentity('ROLE_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_VIEW);

        // insert ACEs for the super admin
        $sid = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_IDDQD);
        $this->aclProvider->updateAcl($acl);

        // AclSecurityIdentity
        try {
            $oid = new ObjectIdentity('class', AclSecurityIdentity::class);
            $acl = $this->aclProvider->createAcl($oid);
        } catch (AclAlreadyExistsException $e) {
            $acl = $this->aclProvider->findAcl($oid);
        }

        $sid = new RoleSecurityIdentity('ROLE_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_VIEW);

        // insert ACEs for the super admin
        $sid = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_IDDQD);
        $this->aclProvider->updateAcl($acl);

        // AccessRule
        try {
            $oid = new ObjectIdentity('class', AccessRule::class);
            $acl = $this->aclProvider->createAcl($oid);
        } catch (AclAlreadyExistsException $e) {
            $acl = $this->aclProvider->findAcl($oid);
        }

        $sid = new RoleSecurityIdentity('ROLE_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_VIEW);

        // insert ACEs for the super admin
        $sid = new RoleSecurityIdentity('ROLE_SUPER_ADMIN');
        $acl->insertClassAce($sid, MaskBuilder::MASK_IDDQD);
        $this->aclProvider->updateAcl($acl);

        $manager->flush();
    }
}
