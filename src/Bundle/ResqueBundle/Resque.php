<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle;

use Psr\Log\NullLogger;

class Resque
{
    /**
     * @var array
     */
    private $kernelOptions;

    /**
     * @var array
     */
    private $redisConfiguration;

    /**
     * @var array
     */
    private $globalRetryStrategy = [];
    /**
     * @var array
     */
    private $jobRetryStrategy = [];

    function __construct(array $kernelOptions)
    {
        $this->kernelOptions = $kernelOptions;
    }

    public function setPrefix($prefix)
    {
        \Resque_Redis::prefix($prefix);
    }

    public function setRedisConfiguration($host, $port, $database)
    {
        $this->redisConfiguration = [
            'host'     => $host,
            'port'     => $port,
            'database' => $database,
        ];

        $host = substr($host, 0, 1) == '/' ? $host : $host.':'.$port;

        \Resque::setBackend($host, $database);
    }

    public function getRedisConfiguration()
    {
        return $this->redisConfiguration;
    }

    public function setGlobalRetryStrategy($strategy)
    {
        $this->globalRetryStrategy = $strategy;
    }

    public function setJobRetryStrategy($strategy)
    {
        $this->jobRetryStrategy = $strategy;
    }

    public function enqueue(AbstractJob $job, $trackStatus = false)
    {
        if ($job instanceof ContainerAwareJob) {
            $job->setKernelOptions($this->kernelOptions);
        }

        $this->attachRetryStrategy($job);

        $result = \Resque::enqueue($job->queue, get_class($job), $job->args, $trackStatus);

        if ($trackStatus) {
            return new \Resque_Job_Status($result);
        }

        return $result;
    }

    public function enqueueOnce(AbstractJob $job, $trackStatus = false)
    {
        $queue = new Queue($job->queue);
        $jobs  = $queue->getJobs();

        foreach ($jobs as $j) {
            if ($j->job->payload['class'] == get_class($job)) {
                if (count(array_intersect($j->args, $job->args)) == count($job->args)) {
                    return ($trackStatus) ? $j->job->payload['id'] : null;
                }
            }
        }

        return $this->enqueue($job, $trackStatus);
    }

    public function enqueueAt($at, AbstractJob $job)
    {
        if ($job instanceof ContainerAwareJob) {
            $job->setKernelOptions($this->kernelOptions);
        }

        $this->attachRetryStrategy($job);
        \ResqueScheduler::enqueueAt($at, $job->queue, get_class($job), $job->args);
        return null;
    }

    public function enqueueIn($in, AbstractJob $job)
    {
        if ($job instanceof ContainerAwareJob) {
            $job->setKernelOptions($this->kernelOptions);
        }

        $this->attachRetryStrategy($job);
        \ResqueScheduler::enqueueIn($in, $job->queue, get_class($job), $job->args);
        return null;
    }

    public function removedDelayed(AbstractJob $job)
    {
        if ($job instanceof ContainerAwareJob) {
            $job->setKernelOptions($this->kernelOptions);
        }

        $this->attachRetryStrategy($job);
        return \ResqueScheduler::removeDelayed($job->queue, get_class($job),$job->args);
    }

    public function removeFromTimestamp($at, AbstractJob $job)
    {
        if ($job instanceof ContainerAwareJob) {
            $job->setKernelOptions($this->kernelOptions);
        }

        $this->attachRetryStrategy($job);
        return \ResqueScheduler::removeDelayedJobFromTimestamp($at, $job->queue, get_class($job), $job->args);
    }

    /**
     * Get queues
     *
     * @return Queue[]
     */
    public function getQueues()
    {
        return \array_map(function ($queue) {
            return new Queue($queue);
        }, \Resque::queues());
    }

    /**
     * @param $queue
     * @return Queue
     */
    public function getQueue($queue)
    {
        return new Queue($queue);
    }

    /**
     * @param $queue
     * @return int Number of queues cleared
     */
    public function clearQueue($queue)
    {
        $length = \Resque::redis()->llen('queue:' . $queue);
        \Resque::redis()->del('queue:' . $queue);

        return $length;
    }

    /**
     * Get workers
     *
     * @return Worker[]
     */
    public function getWorkers()
    {
        return \array_map(function ($worker) {
            return new Worker($worker);
        }, \Resque_Worker::all());
    }

    /**
     * Get worker
     *
     * @param string $id
     * @return Worker
     */
    public function getWorker($id)
    {
        $worker = \Resque_Worker::find($id);

        if(!$worker) {
            return null;
        }

        return new Worker($worker);
    }


    /**
     * Prune dead workers
     */
    public function pruneDeadWorkers()
    {
        // HACK, prune dead workers, just in case
        $worker = new \Resque_Worker('temp');
        $worker->setLogger(new NullLogger());
        $worker->pruneDeadWorkers();
    }

    public function getFailedJobs($offset = -100, $limit = 100)
    {
        return Failure\Redis::all($offset, $limit);
    }

    public function getDelayedJobTimestamps()
    {
        $timestamps = \Resque::redis()->zrange('delayed_queue_schedule', 0, -1);
        //TODO: find a more efficient way to do this
        $out = [];

        foreach ($timestamps as $timestamp) {
            $out[] = [$timestamp, \Resque::redis()->llen('delayed:' . $timestamp)];
        }
        return $out;
    }

    public function getFirstDelayedJobTimestamp()
    {
        $timestamps=$this->getDelayedJobTimestamps();
        if (count($timestamps)>0) {
            return $timestamps[0];
        }

        return [null, 0];
    }

    public function getNumberOfDelayedJobs()
    {
        return \ResqueScheduler::getDelayedQueueScheduleSize();
    }

    public function getJobsForTimestamp($timestamp)
    {
        $jobs = \Resque::redis()->lrange('delayed:' . $timestamp, 0, -1);
        $out  = [];

        foreach ($jobs as $job) {
            $out[] = json_decode($job, true);
        }

        return $out;
    }

    /**
     * Attach any applicable retry strategy to the job.
     *
     * @param AbstractJob $job
     */
    protected function attachRetryStrategy(AbstractJob $job)
    {
        $class = get_class($job);

        if (isset($this->jobRetryStrategy[$class])) {
            if (count($this->jobRetryStrategy[$class])) {
                $job->args['zym_resque.retry_strategy'] = $this->jobRetryStrategy[$class];
            }

            $job->args['zym_resque.retry_strategy'] = $this->jobRetryStrategy[$class];
        } elseif (count($this->globalRetryStrategy)) {
            $job->args['zym_resque.retry_strategy'] = $this->globalRetryStrategy;
        }
    }
}
