<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Repository;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Zym\Bundle\FrameworkBundle\Repository\AbstractEntityRepository;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;

/**
 * @method AclSecurityIdentity|null find($id, $lockMode = null, $lockVersion = null)
 * @method AclSecurityIdentity|null findOneBy(array $criteria, array $orderBy = null)
 * @method AclSecurityIdentity[]    findAll()
 * @method AclSecurityIdentity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AclSecurityIdentityRepository extends AbstractEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AclSecurityIdentity::class);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findAclSecurityIdentities(array $criteria = [], int $page = 1, int $limit = 50, array $orderBy = [])
    {
        $qb = $this->createQueryBuilder('i');
        $this->setQueryBuilderOptions($qb, $criteria, $orderBy);

        $query     = $qb->getQuery();
        $paginator = $this->getPaginator();

        return $paginator->paginate($query, $page, $limit);
    }
}
