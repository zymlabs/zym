<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\FrameworkBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Zym Framework Extension
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class ZymFrameworkExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('zym_framework.form.rest_csrf.cookie.name',   $config['form']['rest_csrf']['cookie']['name']);
        $container->setParameter('zym_framework.form.rest_csrf.cookie.expire', $config['form']['rest_csrf']['cookie']['expire']);
        $container->setParameter('zym_framework.form.rest_csrf.cookie.path',   $config['form']['rest_csrf']['cookie']['path']);
        $container->setParameter('zym_framework.form.rest_csrf.cookie.domain', $config['form']['rest_csrf']['cookie']['domain']);
        $container->setParameter('zym_framework.form.rest_csrf.cookie.secure', $config['form']['rest_csrf']['cookie']['secure']);
        $container->setParameter('zym_framework.form.rest_csrf.header.name',   $config['form']['rest_csrf']['header']['name']);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
    }
}
