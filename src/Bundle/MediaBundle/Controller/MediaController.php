<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\MediaBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\MediaBundle\Entity\Media;
use Zym\Bundle\MediaBundle\Entity\MediaManager;
use Zym\Bundle\MediaBundle\Form\DeleteType;
use Zym\Bundle\MediaBundle\Form\MediaType;
use Zym\Bundle\MediaBundle\MediaPool;

/**
 * Class MediaController
 *
 * @package Zym\Bundle\MediaBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class MediaController extends Controller
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var MediaManager
     */
    private $mediaManager;

    /**
     * @var MediaPool
     */
    private $mediaPool;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        MediaManager $mediaManager,
        MediaPool $mediaPool
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->mediaManager = $mediaManager;
        $this->mediaPool = $mediaPool;
    }

    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_media",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy', []);
        $filterBy = $request->query->get('filterBy', []);

        $medias = $this->mediaManager->findMedias($filterBy, $page, $limit, $orderBy);

        return [
            'medias' => $medias,
        ];
    }

    /**
     * @Route("/add", name="zym_media_add")
     * @Template()
     *
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addAction(Request $request)
    {
        if (!$this->authorizationChecker->isGranted('CREATE', new ObjectIdentity('class', Media::class))) {
            throw new AccessDeniedException();
        }

        $media = new Media();
        $form = $this->createForm(MediaType::class, $media, [
            'provider' => 'zym_media.provider.image',
            'context'  => 'default',
        ]);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->mediaManager->createMedia($media);

                return $this->redirectToRoute('zym_media');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Edit a media
     *
     * @Route("/{id}/edit", name="zym_media_edit")
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @Template()
     *
     * @SecureParam(name="media", permissions="EDIT")
     *
     * @param Request $request
     * @param Media   $media
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, Media $media)
    {
        $origMedia = clone $media;
        $form = $this->createForm(MediaType::class, $media, [
            'provider' => $media->getProviderName(),
            'context'  => $media->getContext(),
        ]);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->mediaManager->saveMedia($media);

                return $this->redirectToRoute('zym_media');
            }
        }

        return [
            'media' => $origMedia,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a media
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_media_delete",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="media", permissions="DELETE")
     *
     * @param Request $request
     * @param Media $media
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request, Media $media)
    {
        $origMedia = clone $media;

        $form = $this->createForm(DeleteType::class, $media);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->mediaManager->deleteMedia($media);

                return $this->redirectToRoute('zym_media');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $this->mediaManager->deleteMedia($media);

            return $this->redirectToRoute('zym_media');
        }

        return [
            'media' => $origMedia,
            'form' => $form->createView(),
        ];
    }

    /**
     * Show a media
     *
     * @Route(
     *     "/{id}.{_format}",
     *     name = "zym_media_show",
     *     defaults = { "_format" = "html" }
     * )
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @Template()
     *
     * @SecureParam(name="media", permissions="VIEW")
     *
     * @param Media $media
     * @return array
     */
    public function showAction(Media $media)
    {
        $provider = $this->mediaPool->getProvider($media->getProviderName());

        return [
            'media'   => $media,
            'formats' => $provider->getFormats(),
        ];
    }

    /**
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @SecureParam(name="media", permissions="VIEW")
     *
     * @param Media $media
     * @param string $format
     * @return Response
     */
    public function viewAction(Media $media, string $format = 'reference')
    {
        return $this->render('ZymMediaBundle:Media:view.html.twig', [
            'media'   => $media,
            'formats' => $this->get('sonata.media.pool')->getFormatNamesByContext($media->getContext()),
            'format'  => $format,
        ]);
    }

    /**
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @SecureParam(name="media", permissions="VIEW")
     *
     * @param Media $media
     * @param string $format
     * @return Response
     */
    public function downloadAction(Media $media, string $format = 'reference')
    {
        $provider = $this->mediaPool->getProvider($media->getProviderName());

        $response = $provider->getDownloadResponse($media, $format, $this->mediaPool->getDownloadMode($media));

        return $response;
    }
}
