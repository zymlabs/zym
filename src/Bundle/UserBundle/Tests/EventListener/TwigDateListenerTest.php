<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Tests\EventListener;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Zym\Bundle\UserBundle\EventListener\TwigDateListener;
use Zym\Bundle\UserBundle\Model\TimeZoneInterface;

/**
 * Zym User Bundle
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class TwigDateListenerTest extends TestCase
{
    /**
     * @var TwigDateListener
     */
    private $listener;

    /**
     * @var \Twig_Environment
     */
    private $twigEnvironment;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->twigEnvironment = $this->createMock(\Twig_Environment::class);
        $this->tokenStorage = $this->createMock(TokenStorageInterface::class);
        $this->listener = new TwigDateListener($this->twigEnvironment, $this->tokenStorage);
    }

    public function testOnKernelRequest()
    {
        $event = $this->createMock(GetResponseEvent::class);
        $token = $this->createMock(TokenInterface::class);
        $user = $this->createMock(TimeZoneInterface::class);
        $extension = new \Twig_Extension_Core();
        $timezone = 'UTC';

        $event->expects($this->once())
            ->method('isMasterRequest')
            ->willReturn(true);

        $this->tokenStorage->expects($this->once())
            ->method('getToken')
            ->willReturn($token);

        $token->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $user->expects($this->once())
            ->method('getTimezone')
            ->willReturn($timezone);

        $this->twigEnvironment->expects($this->once())
            ->method('getExtension')
            ->with(\Twig_Extension_Core::class)
            ->willReturn($extension);

        $this->listener->onKernelRequest($event);

        $this->assertInstanceOf(\DateTimeZone::class, $extension->getTimezone());
    }
}
