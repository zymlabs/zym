<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Tests\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Zym\Bundle\UserBundle\DependencyInjection\Configuration;
use Zym\Bundle\UserBundle\DependencyInjection\ZymUserExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Parser;

/**
 * Class ZymUserExtensionTest
 *
 * @package Zym\Bundle\UserBundle\Tests\DependencyInjection
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ZymUserExtensionTest extends TestCase
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        unset($this->configuration);
    }

    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     */
    public function testUserLoadThrowsExceptionUnlessDatabaseDriverSet()
    {
        $loader = new ZymUserExtension();
        $config = $this->getEmptyConfig();
        unset($config['db_driver']);
        $loader->load([$config], new ContainerBuilder());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUserLoadThrowsExceptionUnlessDatabaseDriverIsValid()
    {
        $loader = new ZymUserExtension();
        $config = $this->getEmptyConfig();
        $config['db_driver'] = 'foo';
        $loader->load([$config], new ContainerBuilder());
    }

    /**
     * @return ContainerBuilder
     */
    protected function createEmptyConfiguration()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new ZymUserExtension();
        $config = $this->getEmptyConfig();
        $loader->load([$config], $this->configuration);
        $this->assertInstanceOf(ContainerBuilder::class, $this->configuration);
    }

    /**
     * @return ContainerBuilder
     */
    protected function createFullConfiguration()
    {
        $this->configuration = new ContainerBuilder();
        $loader = new ZymUserExtension();
        $config = $this->getFullConfig();
        $loader->load([$config], $this->configuration);
        $this->assertInstanceOf(ContainerBuilder::class, $this->configuration);
    }

    /**
     * @return array
     */
    private function getEmptyConfig()
    {
        $yaml = <<<EOF
db_driver: mongodb
EOF;
        $parser = new Parser();

        return $parser->parse($yaml);
    }

    /**
     * @return array
     */
    private function getFullConfig()
    {
        $yaml = <<<EOF
db_driver: orm
EOF;
        $parser = new Parser();

        return $parser->parse($yaml);
    }
}
