<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Manager;

use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\GroupManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\UserBundle\Repository\GroupRepository;

/**
 * Class GroupManager
 *
 * @package Zym\Bundle\UserBundle\Entity
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class GroupManager extends AbstractEntityManager implements GroupManagerInterface
{
    /**
     * @var GroupRepository
     */
    protected $repository;

    /**
     * {@inheritdoc}
     */
    public function createGroup($name)
    {
        $class = $this->getClass();

        return new $class($name);
    }

    /**
     * {@inheritdoc}
     */
    public function findGroupByName($name)
    {
        return $this->findGroupBy(['name' => $name]);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteGroup(GroupInterface $group)
    {
        $this->entityManager->remove($group);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * {@inheritdoc}
     */
    public function findGroupBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function findGroups()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findGroupsPaginated(array $criteria = [], int $page = 1, int $limit = 50, array $orderBy = [])
    {
        return $this->repository->findGroups($criteria, $page, $limit, $orderBy);

    }

    /**
     * {@inheritdoc}
     */
    public function updateGroup(GroupInterface $group, bool $andFlush = true)
    {
        $this->entityManager->persist($group);
        if ($andFlush) {
            $this->entityManager->flush();
        }
    }
}
