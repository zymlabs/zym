<?php

namespace Zym\Bundle\UserBundle\Util;

/**
 * GUID Generator
 *
 * @see http://php.net/manual/en/function.com-create-guid.php#119168
 */
class GuidGenerator
{
    /**
     * Returns a GUIDv4 string
     *
     * Uses a cryptographically secure method, if available.
     * Otherwise, fallback uses an older, less secure version.
     *
     * @return string
     */
    public static function generateGuid()
    {
        // Requires OpenSSL PHP extension
        if (function_exists('openssl_random_pseudo_bytes')) {
            $data = openssl_random_pseudo_bytes(16);
            $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // set version to 0100
            $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // set bits 6-7 to 10
            return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
        }

        // Fallback (PHP 4.2+)
        mt_srand((double)microtime() * 10000);
        $str = strtolower(md5(uniqid(rand(), true)));
        return sprintf('%s-%s-%s-%s-%s',
            substr($str, 0, 8),
            substr($str, 8, 4),
            substr($str, 12, 4),
            substr($str, 16, 4),
            substr($str, 20, 12)
        );
    }
}
