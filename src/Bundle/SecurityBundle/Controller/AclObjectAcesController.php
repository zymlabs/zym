<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\EntryInterface;
use Symfony\Component\Security\Acl\Model\MutableAclInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Form\AclEntryType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;
use Zym\Bundle\SecurityBundle\Manager\AclClassManager;

/**
 * Acl Object Aces Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AclObjectAcesController extends Controller
{
    /**
     * @var AclClassManager
     */
    private $aclClassManager;

    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        AclClassManager $aclClassManager,
        AclProviderInterface $aclProvider,
        AuthorizationCheckerInterface $authorizationChecker,
        TranslatorInterface $translator
    ) {
        $this->aclClassManager = $aclClassManager;
        $this->aclProvider = $aclProvider;
        $this->authorizationChecker = $authorizationChecker;
        $this->translator = $translator;
    }

    /**
     * @Route(
     *     "{type}/{identifier}.{_format}",
     *     name="zym_security_acl_object_aces",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     * @param string $identifier
     * @param string $type
     * @return array
     */
    public function listAction(Request $request, $identifier, $type)
    {
        $oid = new ObjectIdentity($identifier, $type);
        $acl = $this->aclProvider->findAcl($oid);

        if (!$acl) {
            throw $this->createNotFoundException('Index does not exist');
        }

        return [
            'acl' => $acl,
            'oid' => $oid,
        ];
    }

    /**
     * @Route("/add/{classType}", name="zym_security_acl_entries_add")
     * @Template()
     *
     * @param Request $request
     * @param AclClass $aclClass
     * @return array|RedirectResponse
     */
    public function addAction(Request $request, AclClass $aclClass)
    {
        if (!$this->authorizationChecker->isGranted('OPERATOR', new ObjectIdentity('class', $aclClass->getClassType()))) {
            throw new AccessDeniedException();
        }

        $oid = new ObjectIdentity('class', $aclClass->getClassType());

        /** @var MutableAclInterface $acl */
        $acl = $this->aclProvider->findAcl($oid);

        if (!$acl) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $form = $this->createForm(AclEntryType::class, $aclClass);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $oid = $form->get('securityIdentity')->getData();
                $mask = $form->get('mask')->getData();
                $acl->insertClassAce($oid, $mask);

                $this->aclProvider->updateAcl($acl);
                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{classType}/edit/{index}", name="zym_security_acl_entries_edit")
     * @Template()
     *
     * @SecureParam(name="aclClass", permissions="EDIT")
     *
     * @param Request $request
     * @param AclClass $aclClass
     * @param int $index
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, AclClass $aclClass, $index)
    {
        $origAclClass = clone $aclClass;

        $oid = new ObjectIdentity('class', $aclClass->getClassType());

        /** @var MutableAclInterface $acl */
        $acl = $this->aclProvider->findAcl($oid);

        $classAces = $acl->getClassAces();

        if (!isset($classAces[$index])) {
            throw $this->createNotFoundException('Index does not exist');
        }

        /** @var EntryInterface $classAce */
        $classAce = clone $classAces[$index];

        $form = $this->createForm(AclEntryType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->updateClassAce($index, $classAce->getMask());

                $this->aclProvider->updateAcl($acl);

                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'aclClass' => $origAclClass,
            'index'    => $index,
            'form'     => $form->createView(),
        ];
    }

    /**
     * Delete a aclClass
     *
     * @Route(
     *     "{type}/{identifier}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "{type}/{identifier}/delete.{_format}",
     *     name="zym_security_acl_object_aces_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="aclClass", permissions="DELETE")
     * @param Request $request
     * @param AclClass $aclClass
     * @param string $identifier
     * @return array|RedirectResponse
     */
    public function deleteAction(Request $request, AclClass $aclClass, $identifier)
    {
        $origAclClass = clone $aclClass;

        $form = $this->createForm(DeleteType::class);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $oid = new ObjectIdentity('class', $aclClass->getClassType());

                /** @var MutableAclInterface $acl */
                $acl = $this->aclProvider->findAcl($oid);

                $classAces = $acl->getClassAces();

                if (!isset($classAces[$identifier])) {
                    throw $this->createNotFoundException('Index does not exist');
                }

                $this->aclClassManager->deleteAclClass($aclClass);

                $this->addFlash('success', $this->translator->trans('Role Deleted'));

                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $this->aclClassManager->deleteNode($aclClass);

            return $this->redirectToRoute('zym_security_acl_entries');
        }

        return [
            'aclClass' => $origAclClass,
            'form' => $form->createView(),
        ];
    }
}
