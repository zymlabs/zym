<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ThemeBundle\Manager;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\ThemeBundle\Entity\ThemeRule;
use Zym\Bundle\ThemeBundle\Repository\ThemeRuleRepository;

/**
 * Class ThemeRuleManager
 *
 * @package Zym\Bundle\ThemeBundle\Entity
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ThemeRuleManager extends AbstractEntityManager
{
    /**
     * @var ThemeRuleRepository
     */
    protected $repository;

    /**
     * @param ThemeRule $themeRule
     * @return ThemeRule
     */
    public function createThemeRule(ThemeRule $themeRule)
    {
        return $this->createEntity($themeRule);
    }

    /**
     * @param ThemeRule $themeRule
     */
    public function deleteThemeRule(ThemeRule $themeRule)
    {
        $this->deleteEntity($themeRule);
    }

    /**
     * @param ThemeRule $themeRule
     * @param bool $andFlush
     * @return ThemeRule
     */
    public function saveThemeRule(ThemeRule $themeRule, bool $andFlush = true)
    {
        return $this->saveEntity($themeRule, $andFlush);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findThemeRules(array $criteria = null, $page = 1, $limit = 50, array $orderBy = null)
    {
        return $this->repository->findThemeRules($criteria, $page, $limit, $orderBy);
    }

    /**
     * @param array $criteria
     * @return ThemeRule|null
     */
    public function findThemeRuleBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @return ThemeRule[]
     */
    public function getRules()
    {
        return $this->repository->findAll();
    }
}
