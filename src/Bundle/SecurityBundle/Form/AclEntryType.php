<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Acl\Model\EntryInterface;

class AclEntryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('securityIdentity', AclSecurityIdentityEntityType::class, [
                'label' => 'Security Identity',
                'property_path' => ($options['data'] instanceof EntryInterface) ? 'securityIdentity.role' : 'securityIdentity',
                'multiple' => false,
                'read_only' => ($options['data'] instanceof EntryInterface),
            ])
            ->add('mask', PermissionMaskType::class, [
                'label' => 'Permission Mask',
            ])
            ->add('granting', ChoiceType::class, [
                'choices' => [
                    'Deny' => false,
                    'Allow' => true,
                ],
                'choices_as_values' => true,
                'read_only' => true,
            ])
            ->add('strategy', ChoiceType::class, [
                'choices' => [
                    'Equal' => 'equal',
                    'All' => 'all',
                    'Any' => 'any',
                ],
                'choices_as_values' => true,
                'preferred_choices' => ['all'],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
