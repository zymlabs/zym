<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RouterBundle\Repository;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Zym\Bundle\FrameworkBundle\Repository\AbstractEntityRepository;
use Zym\Bundle\RouterBundle\Entity\Route;

/**
 * @method Route|null find($id, $lockMode = null, $lockVersion = null)
 * @method Route|null findOneBy(array $criteria, array $orderBy = null)
 * @method Route[]    findAll()
 * @method Route[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouteRepository extends AbstractEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Route::class);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findRoutes(array $criteria = [], int $page = 1, int $limit = 10, array $orderBy = [])
    {
        $qb = $this->createQueryBuilder('r');
        $this->setQueryBuilderOptions($qb, $criteria, $orderBy);

        $query     = $qb->getQuery();
        $paginator = $this->getPaginator();

        return $paginator->paginate($query, $page, $limit);
    }

    public function findLatestUpdatedTimestamp()
    {
        $qb = $this->createQueryBuilder('r');
        $qb->select('r.updatedAt')
           ->orderBy('r.updatedAt', 'DESC')
           ->setMaxResults(1);

        $query = $qb->getQuery();
        $time = $query->getSingleScalarResult();

        return $time;
    }
}
