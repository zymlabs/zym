<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\VirtualProperty;

class Worker
{
    /**
     * @var \Resque_Worker
     *
     * @Exclude
     */
    protected $worker;

    public function __construct(\Resque_Worker $worker)
    {
        $this->worker = $worker;
    }

    /**
     * Get the worker id
     *
     * @return string
     *
     * @VirtualProperty
     */
    public function getId()
    {
        return (string) $this->worker;
    }

    /**
     * Get the worker hostname
     *
     * @return string
     *
     * @VirtualProperty
     */
    public function getHostname()
    {
        return current(explode(':', $this->worker));
    }

    public function stop()
    {
        $parts = explode(':', $this->getId());

        posix_kill($parts[1], 3);
    }

    /**
     * Get the worker queues
     *
     * @return string
     *
     * @VirtualProperty
     */
    public function getQueues()
    {
        return array_map(function ($queue) {
            return new Queue($queue);
        }, $this->worker->queues());
    }

    /**
     * @VirtualProperty
     */
    public function getProcessedCount()
    {
        return $this->worker->getStat('processed');
    }

    /**
     * @VirtualProperty
     */
    public function getFailedCount()
    {
        return $this->worker->getStat('failed');
    }

    /**
     * @VirtualProperty
     */
    public function getCurrentJobStartedAt()
    {
        $job = $this->worker->job();

        if (!$job) {
            return null;
        }

        return new \DateTime($job['run_at']);
    }

    /**
     * @VirtualProperty
     */
    public function getCurrentJob()
    {
        $job = $this->worker->job();

        if (!$job) {
            return null;
        }

        $job = new \Resque_Job($job['queue'], $job['payload']);

        $instance = $job->getInstance();
        $instance->worker = $this;

        return $instance;
    }
}
