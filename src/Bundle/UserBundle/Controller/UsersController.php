<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\View\View as ViewResponse;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Zym\Bundle\UserBundle\Entity\User;
use Zym\Bundle\UserBundle\Form\DeleteType;
use Zym\Bundle\UserBundle\Form\EditUserType;
use Zym\Bundle\UserBundle\Form\UserType;
use Zym\Bundle\UserBundle\Manager\UserManager;

/**
 * Class UsersController
 *
 * @package Zym\Bundle\UserBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class UsersController extends Controller
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, UserManager $userManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->userManager = $userManager;
    }

    /**
     * Layout
     *
     * @Route(
     *     "layout.{_format}",
     *     name="zym_user_users_layout",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     * @View()
     */
    public function layoutAction()
    {
        return [];
    }

    /**
     * List all users
     *
     * @Route(
     *     ".{_format}",
     *     name="zym_user_users",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     * @View()
     *
     * @ApiDoc(
     *     description="Returns a list users",
     *     section="Users",
     *     filters={
     *         {"name"="page", "dataType"="integer"},
     *         {"name"="limit", "dataType"="integer"},
     *         {"name"="orderBy", "dataType"="array"},
     *         {"name"="filterBy", "dataType"="array"}
     *     }
     * )
     * @param Request $request
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $users = $this->userManager->findUsersPaginated($filterBy, $page, $limit, $orderBy);

        return [
            'users' => $users,
        ];
    }

    /**
     * @deprecated
     *
     * @Route("/create", name="zym_user_users_create")
     * @Template()
     */
    public function createAction()
    {
        return $this->forward('ZymUserBundle:Users:add');
    }

    /**
     * Add a user
     *
     * @Route(
     *     "/add.{_format}",
     *     name="zym_user_users_add",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     ".{_format}",
     *     name="zym_user_users_post_add",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     methods={"POST"}
     * )
     *
     * @View()
     * @ApiDoc(
     *     description="Add a user",
     *     section="Users",
     *     parameters={
     *         {"name"="email", "dataType"="string", "required"=true, "description"="Email address", "readonly"=false},
     *         {"name"="plainPassword[password]", "dataType"="string", "required"=true, "description"="Password", "readonly"=false},
     *         {"name"="plainPassword[confirmPassword]", "dataType"="string", "required"=true, "description"="Confirm Password", "readonly"=false}
     *     }
     * )
     * @param Request $request
     *
     * @return array|ViewResponse
     */
    public function addAction(Request $request)
    {
        if (!$this->authorizationChecker->isGranted('CREATE', new ObjectIdentity('class', User::class))) {
            throw new AccessDeniedHttpException();
        }

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setConfirmationToken(null);

            $this->userManager->updateUser($user);

            return ViewResponse::createRouteRedirect(
                    'zym_user_users_show',
                    [
                        'id'      => $user->getId(),
                        '_format' => $request->getRequestFormat(),
                    ],
                    Response::HTTP_CREATED
                )
                ->setData([
                    'user' => $user,
                ]);
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Show a user
     *
     * @param User $user
     *
     * @return array
     *
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_users_show",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"GET"},
     *     options={"expose" = true}
     * )
     * @ParamConverter("user", class="ZymUserBundle:User")
     * @View()
     *
     * @SecureParam(name="user", permissions="VIEW")
     *
     * @ApiDoc(
     *     description="Returns a user",
     *     section="Users"
     * )
     */
    public function showAction(User $user)
    {
        return [
            'user' => $user,
        ];
    }

    /**
     * Edit a user
     *
     * @param Request $request
     * @param User    $user
     *
     * @return array
     * @Route(
     *     "/{id}/edit.{_format}",
     *     name="zym_user_users_edit",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_users_put_edit",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"PUT"}
     * )
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_users_patch_edit",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"PATCH"}
     * )
     *
     * @ParamConverter("user", class="ZymUserBundle:User")
     * @View()
     *
     * @SecureParam(name="user", permissions="EDIT")
     *
     * @ApiDoc(
     *     description="Edit a user",
     *     section="Users",
     *     parameters={
     *         {"name"="email", "dataType"="string", "required"=true, "description"="Email address", "readonly"=false},
     *         {"name"="plainPassword[password]", "dataType"="string", "required"=true, "description"="Password", "readonly"=false},
     *         {"name"="plainPassword[confirmPassword]", "dataType"="string", "required"=true, "description"="Confirm Password", "readonly"=false}
     *     }
     * )
     */
    public function editAction(Request $request, User $user)
    {
        $originalUser = clone $user;
        $form = $this->createForm(EditUserType::class, $user, [
            'method' => in_array($request->getMethod(), [Request::METHOD_PUT, Request::METHOD_PATCH]) ? $request->getMethod() : Request::METHOD_POST
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userManager->updateUser($user);

            return ViewResponse::createRouteRedirect(
                    'zym_user_users_show',
                    ['id' => $user->getId()],
                    Response::HTTP_OK
                )
                ->setData([
                    'user' => $user,
                ]);
        }

        return [
            'user' => $originalUser,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a user
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_user_users_delete",
     *     defaults={ "_format" = "html" },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"GET", "POST"},
     *     options={"expose" = true}
     * )
     * @Route(
     *     "/{id}.{_format}",
     *     name="zym_user_users_delete_delete",
     *     defaults={ "_format" = "html" },
     *     requirements = {
     *         "id" = "\d+"
     *     },
     *     methods={"DELETE"}
     * )
     *
     * @SecureParam(name="user", permissions="DELETE")
     *
     * @View()
     * @ApiDoc(
     *     description="Delete a user",
     *     section="Users"
     * )
     *
     * @param Request $request
     * @param User    $user
     * @return array
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createForm(DeleteType::class, $user, [
            'method' => $request->isMethod(Request::METHOD_DELETE) ? $request->getMethod() : Request::METHOD_POST
        ]);

        $form->handleRequest($request);

        if (($form->isSubmitted() && $form->isValid()) || $request->isMethod(Request::METHOD_DELETE)) {
            $this->userManager->deleteUser($user);

            return ViewResponse::createRouteRedirect('zym_user_users', [], Response::HTTP_OK);
        }

        return [
            'user' => $user,
            'form' => $form->createView(),
        ];
    }
}
