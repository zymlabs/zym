<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RuntimeConfigBundle\Repository;

use Knp\Component\Pager\Pagination\PaginationInterface;
use OpenSky\Bundle\RuntimeConfigBundle\Model\ParameterProviderInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Zym\Bundle\FrameworkBundle\Repository\AbstractEntityRepository;
use Zym\Bundle\RuntimeConfigBundle\Entity\Parameter;

/**
 * @method Parameter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Parameter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Parameter[]    findAll()
 * @method Parameter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParameterRepository extends AbstractEntityRepository implements ParameterProviderInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Parameter::class);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findParameters(array $criteria = [], int $page = 1, int $limit = 10, array $orderBy = [])
    {
        $qb = $this->createQueryBuilder('p');
        $this->setQueryBuilderOptions($qb, $criteria, $orderBy);

        $query     = $qb->getQuery();
        $paginator = $this->getPaginator();

        return $paginator->paginate($query, $page, $limit);
    }

    /**
     * @see \OpenSky\Bundle\RuntimeConfigBundle\Model\ParameterProviderInterface::getParametersAsKeyValueHash()
     */
    public function getParametersAsKeyValueHash()
    {
        $results = $this->createQueryBuilder('p')
            ->select('p.name', 'p.value')
            ->getQuery()
            ->getResult();

        $parameters = [];

        foreach ($results as $result) {
            $parameters[$result['name']] = $result['value'];
        }

        return $parameters;
    }
}
