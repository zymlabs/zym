<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RouterBundle\Manager;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;
use Zym\Bundle\RouterBundle\Entity\Route;
use Zym\Bundle\RouterBundle\Repository\RouteRepository;

/**
 * Route Manager
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class RouteManager extends AbstractEntityManager
{
    /**
     * @var RouteRepository
     */
    protected $repository;

    /**
     * @param Route $route
     * @return Route
     */
    public function createRoute(Route $route)
    {
        return $this->createEntity($route);
    }

    /**
     * @param Route $route
     */
    public function deleteRoute(Route $route)
    {
        $this->deleteEntity($route);
    }

    /**
     * @param Route $route
     * @param bool $andFlush
     * @return Route
     */
    public function saveRoute(Route $route, bool $andFlush = true)
    {
        return $this->saveEntity($route, $andFlush);
    }

    /**
     * @param array $criteria
     * @return Route|null
     */
    public function findRouteBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param array $criteria
     * @return bool
     */
    public function hasRouteBy(array $criteria)
    {
        return $this->repository->hasRouteBy($criteria);
    }

    /**
     * @param array $criteria
     * @param int   $page
     * @param int   $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findRoutes(array $criteria = null, $page = 1, $limit = 10,array $orderBy = null)
    {
        $entities = $this->repository->findRoutes($criteria, $page, $limit, $orderBy);
        $this->loadAcls($entities);

        return $entities;
    }
}
